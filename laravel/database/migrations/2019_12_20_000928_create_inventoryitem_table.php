

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInventoryitemTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inventoryitem', function (Blueprint $table) {
            $table->integer('itemid');
            $table->integer('amountexisting');
            $table->decimal('amountshown', 10, 0)->comment('can be percentage in decimal or a cieling number (for now a 1 is a 100% and not 1 item)');
            $table->decimal('pricebought', 10, 2);
            $table->decimal('priceselling', 10, 2);
            $table->string('name', 20);
            $table->string('description', 2000);
            $table->string('gallery', 100);
            $table->date('expirydate');
            $table->date('datebought');
            $table->integer('catid')->comment('for category');
            $table->integer('iid')->comment('inventory foreign key');
            $table->decimal('CompetitorPrice', 10, 2);
            $table->integer('MinimumStockLevel');
            $table->timestamps();
            $table->index(["catid"]);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('inventoryitem');
    }
}

