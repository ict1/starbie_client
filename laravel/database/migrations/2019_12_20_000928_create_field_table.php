

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFieldTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('field', function (Blueprint $table) {
            $table->integer('fieldid');
            $table->integer('forbranch')->comment('0 or 1');
            $table->integer('forcust')->comment('0 or 1');
            $table->integer('forinvitem')->comment('0 or 1');
            $table->string('fieldname', 20);
            $table->string('fieldtype', 20);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('field');
    }
}

