

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrganizationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('organization', function (Blueprint $table) {
            $table->integer('oid');
            $table->string('name', 50);
            $table->string('logo', 100);
            $table->string('themecolor', 20);
            $table->string('contactno1', 20);
            $table->string('contactno2', 20);
            $table->string('headquarter', 11);
            $table->string('about', 2000);
            $table->integer('mustreg')->comment('0 or 1');
            $table->integer('canreg')->comment('0 or 1');
            $table->integer('mustapprove')->comment('0 or 1');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('organization');
    }
}

