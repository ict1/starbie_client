

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransactionitemTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactionitem', function (Blueprint $table) {
            $table->integer('itemid');
            $table->integer('tid');
            $table->integer('amount');
            $table->decimal('totalprice', 10, 0);
            $table->timestamps();
            $table->index(["tid"]);
            $table->index(["itemid"]);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transactionitem');
    }
}

