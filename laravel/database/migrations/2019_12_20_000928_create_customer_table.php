

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customer', function (Blueprint $table) {
            $table->increments('cid');
            $table->string('fname', 20);
            $table->string('mnames', 50)->nullable();
            $table->string('lname', 20);
            $table->date('dob')->nullable();
            $table->string('gender', 1)->nullable();
            $table->integer('addressid')->nullable();
            $table->integer('paddressid')->nullable();
            $table->string('password', 50);
            $table->integer('approved')->comment('0 or 1')->default(1);
            $table->integer('session')->comment('0 or 1')->default(0);
            $table->string('profilepic', 100)->nullable();
            $table->string('email', 100)->unique();
            $table->integer('oid');
            $table->string('streetnumber', 20)->nullable();
            $table->timestamps();
            $table->index(["addressid"]);
            $table->index(["paddressid"]);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customer');
    }
}

