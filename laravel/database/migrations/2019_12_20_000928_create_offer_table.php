

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOfferTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('offer', function (Blueprint $table) {
            $table->integer('offerid');
            $table->integer('forbranch');
            $table->integer('forcat');
            $table->integer('foritem');
            $table->integer('bid');
            $table->integer('itemid');
            $table->integer('catid');
            $table->string('offername', 20);
            $table->string('offerdetails', 2000);
            $table->string('offerposter', 100);
            $table->integer('isdiscount')->comment('0 or 1');
            $table->decimal('discount percentage', 10, 0);
            $table->timestamps();
            $table->index(["bid"]);
            $table->index(["itemid"]);
            $table->index(["catid"]);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('offer');
    }
}

