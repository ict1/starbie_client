

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReviewTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('review', function (Blueprint $table) {
            $table->integer('cid');
            $table->integer('tid')->comment('FK transaction item');
            $table->integer('itemid')->comment('FK transaction item');
            $table->string('comment', 1000);
            $table->timestamps();
            $table->index(["cid"]);
            $table->index(["tid"]);
            $table->index(["itemid"]);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('review');
    }
}

