

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOreviewTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('oreview', function (Blueprint $table) {
            $table->integer('cid');
            $table->integer('oid');
            $table->string('comment', 1000);
            $table->timestamps();
            $table->index(["cid"]);
            $table->index(["oid"]);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('oreview');
    }
}

