

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFieldfilledTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fieldfilled', function (Blueprint $table) {
            $table->integer('fieldid');
            $table->integer('bid');
            $table->integer('itemid');
            $table->integer('cid');
            $table->string('information', 1000);
            $table->timestamps();
            $table->index(["fieldid"]);
            $table->index(["bid"]);
            $table->index(["itemid"]);
            $table->index(["cid"]);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fieldfilled');
    }
}

