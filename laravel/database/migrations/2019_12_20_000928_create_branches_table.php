

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBranchesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('branches', function (Blueprint $table) {
            $table->integer('bid');
            $table->integer('oid');
            $table->integer('addressid');
            $table->integer('postaladdressid');
            $table->string('pobox', 6);
            $table->string('contactno', 20);
            $table->string('about', 2000);
            $table->string('gallery', 100);
            $table->string('street_num', 10);
            $table->timestamps();
            $table->index(["oid"]);
            $table->index(["addressid"]);
            $table->index(["postaladdressid"]);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('branches');
    }
}

