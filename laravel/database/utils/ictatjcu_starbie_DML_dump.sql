-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Dec 19, 2019 at 10:07 AM
-- Server version: 5.6.40-84.0-log
-- PHP Version: 7.3.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ictatjcu_starbie`
--

--
-- Dumping data for table `address`
--

INSERT INTO `address` (`adid`, `streetname`, `postcode`, `log`, `lat`, `country`, `city`) VALUES
(1951, 'Victoria street', '4590', NULL, NULL, 'USA', 'New Yourk'),
(1952, 'Mandela street', '4590', NULL, NULL, 'Kenya', 'Nairobi'),
(1953, 'Stella', '098', NULL, NULL, 'Australia', 'Sydney');

--
-- Dumping data for table `branches`
--

INSERT INTO `branches` (`bid`, `oid`, `street_num`, `postaladdressid`, `pobox`, `contactno`, `about`, `gallery`, `addressid`) VALUES
(1, 1, '5', 2, '123', '03773567', 'Branch of the east side', NULL, NULL),
(2, 1, '5', 2, '123', '03773567', 'Branch of the west side', NULL, NULL),
(3, 3, '5', 2, '123', '03773567', 'Branch of the west side', NULL, NULL),
(4, 3, '5', 2, '123', '03773567', 'Branch of the west side', NULL, NULL),
(5, 5, '5', 2, '123', '03773567', 'Branch of the west side', 'branch1.jpg', 1952),
(12, 7, '1', 12121212, '1111', '8787878787', 'Branch of the east side', 'branch1.jpg', 1951),
(13, 7, '2', 8787, '6565', '8989898989', 'Branch of the west side', 'branch2.jpg', 1952),
(14, 7, '2', 121212, '1111', '2323323232', 'Branch of the south side', 'branch3.jpg', 1953),
(15, 6, '1', 121212, '2222', '4545454545', 'Branch of the north side', NULL, 1952);

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`catid`, `iid`, `name`, `description`) VALUES
(1, 1, 'Fruit and vegetable', ''),
(2, 2, 'Stationary', ''),
(3, 2, 'Drink', ''),
(4, 2, 'Clothing and Accesso', ''),
(5, 3, 'Electronics', ''),
(21, 18, 'Coffee', ''),
(22, 19, 'Drinks', ''),
(23, 20, 'Chocolates', '');

--
-- Dumping data for table `contact`
--

INSERT INTO `contact` (`contid`, `name`, `email`, `message`, `oid`) VALUES
(3, 'Chris Hemsworth', 'abc@xyz.com', ' Test', 0);

--
-- Dumping data for table `customer`
--

INSERT INTO `customer` (`cid`, `fname`, `mnames`, `lname`, `dob`, `gender`, `addressid`, `paddressid`, `password`, `approved`, `session`, `profilepic`, `email`, `oid`, `streetnumber`) VALUES
(1, 'Alisson', 'Pessanha', 'Doan', '2019-01-08', 'M', NULL, NULL, NULL, NULL, NULL, NULL, '', 0, '');

--
-- Dumping data for table `inventory`
--

INSERT INTO `inventory` (`iid`, `bid`, `addressid`, `pobox`, `contactno`) VALUES
(1, 5, 1, '7786', '6178695043'),
(2, 4, 7, 'PO7865', '9087689765'),
(3, 1, 2, '9087', '9088997657'),
(6, 12, 1951, '11111', '98989898989'),
(7, 13, 1952, '11111', '2323232323'),
(8, 14, 1953, '11111', '4545454545');

--
-- Dumping data for table `inventoryitem`
--

INSERT INTO `inventoryitem` (`itemid`, `amountexisting`, `amountshown`, `pricebought`, `priceselling`, `name`, `description`, `gallery`, `expirydate`, `datebought`, `catid`, `iid`, `CompetitorPrice`, `MinimumStockLevel`) VALUES
(1, 400, '320', 0.7, 1.0, 'Fresh Pink Lady Appl', '', '', '2019-11-30', '2019-09-10', 1, 1, 1.2, 500),
(2, 400, '320', 0.7, 1.0, 'Fresh Royal Gala App', '', '', '2019-11-30', '2019-09-10', 1, 1, 1.2, 600),
(3, 400, '320', 0.7, 1.0, 'Fresh Granny Smith A', '', '', '2019-11-30', '2019-09-10', 1, 1, 1.2, 700),
(4, 400, '320', 0.7, 1.0, 'Fresh Packham Pear', '', '', '2019-11-30', '2019-09-10', 1, 1, 1.2, 800),
(5, 100, '80', 4.9, 7.0, 'Grapes Red Seedless ', '', '', '2019-11-30', '2019-09-10', 1, 1, 6, 120),
(6, 100, '80', 6.3, 9.0, 'Black Seedless Grape', '', '', '2019-11-30', '2019-09-10', 1, 1, 5.9, 150),
(7, 100, '80', 5.6, 8.0, 'White Seedless Grape', '', '', '2019-11-30', '2019-09-10', 1, 1, 5, 100),
(8, 100, '80', 7.0, 10.0, 'Grape Red Globe 1kg ', '', '', '2019-11-25', '2019-09-10', 1, 1, 7.5, 200),
(9, 100, '80', 2.1, 3.0, 'Coconut Green For Dr', '', '', '2019-11-25', '2019-09-10', 1, 1, 1.8, 200),
(10, 300, '240', 2.8, 4.0, 'Cabbage Green whole ', '', '', '2019-11-25', '2019-09-10', 1, 1, 3.5, 500),
(11, 300, '240', 3.5, 5.0, 'Snow Peas Fresh 250g', '', '', '2019-11-25', '2019-09-10', 1, 1, 3, 500),
(12, 300, '240', 2.8, 4.0, 'Fresh Rockmelon Whol', '', '', '2019-11-25', '2019-09-10', 1, 1, 3, 500),
(13, 1000, '800', 2.8, 4.0, 'Brown Paper Craft Ro', '', '', '0000-00-00', '2019-08-10', 2, 2, 3, 600),
(14, 1000, '800', 2.8, 4.0, 'Poly Craft String Wh', '', '', '0000-00-00', '2019-08-10', 2, 2, 3, 700),
(15, 1000, '800', 7.0, 10.0, 'Paperclick A3 Visual', '', '', '0000-00-00', '2019-08-10', 2, 2, 9, 800),
(16, 1000, '800', 2.1, 3.0, 'Pilot Frixion Ballpo', '', '', '0000-00-00', '2019-08-10', 2, 2, 3, 900),
(17, 1000, '800', 2.8, 4.0, 'Sharpie Marker Fine ', '', '', '0000-00-00', '2019-08-10', 2, 2, 4.2, 1000),
(18, 1000, '800', 2.8, 4.0, 'Woolworths Lever Arc', '', '', '0000-00-00', '2019-08-10', 2, 2, 3.5, 1100),
(19, 1000, '800', 4.9, 7.0, 'Staedtler Noris Club', '', '', '0000-00-00', '2019-08-10', 2, 2, 6, 600),
(20, 1000, '800', 35.0, 50.0, 'Dymo Letratag Labell', '', '', '0000-00-00', '2019-08-10', 2, 2, 42, 500),
(21, 1000, '800', 1.4, 2.0, 'Paperclick Alphabet ', '', '', '0000-00-00', '2019-08-10', 2, 2, 2, 500),
(22, 600, '480', 4.9, 7.0, 'Coca Cola Mini Cans ', '', '', '0000-00-00', '2019-08-10', 3, 2, 6, 700),
(23, 600, '480', 3.5, 5.0, 'Bel Normande Sparkli', '', '', '0000-00-00', '2019-08-10', 3, 2, 4.5, 650),
(24, 600, '480', 4.2, 6.0, 'Good Earth Kombucha ', '', '', '0000-00-00', '2019-08-10', 3, 2, 7, 750),
(25, 600, '480', 2.1, 3.0, 'Belvoir Raspberry Le', '', '', '2020-01-08', '2019-11-10', 3, 2, 2.7, 500),
(26, 600, '480', 2.1, 3.0, 'Solo Lemon Zero Suga', '', '', '2020-03-20', '2019-11-10', 3, 2, 2.7, 500),
(27, 600, '480', 2.8, 4.0, 'Sprite Bottle', '', '', '2020-05-19', '2019-11-10', 3, 2, 4.5, 1000),
(28, 600, '480', 2.8, 4.0, 'Fanta Orange Bottle', '', '', '2020-04-20', '2019-11-10', 3, 2, 4, 1000),
(29, 600, '480', 2.8, 4.0, 'Coca Cola No Sugar B', '', '', '2020-08-16', '2019-11-10', 3, 2, 4, 1000),
(30, 600, '480', 4.2, 6.0, 'Bundaberg Tropical M', '', '', '2019-12-30', '2019-11-10', 3, 2, 6, 1000),
(31, 100, '80', 6.3, 9.0, 'Bonds Womens Underwe', '', '', '0000-00-00', '2019-07-10', 4, 2, 8.5, 50),
(32, 100, '80', 2.8, 4.0, 'Clio Girl Summer Glo', '', '', '0000-00-00', '2019-07-10', 4, 2, 3.2, 50),
(33, 100, '80', 2.8, 4.0, 'Clio Invisible Band ', '', '', '0000-00-00', '2019-07-10', 4, 2, 2, 120),
(34, 100, '80', 12.6, 18.0, 'Umbrella Mens Big Br', '', '', '0000-00-00', '2019-07-10', 4, 2, 12, 120),
(35, 100, '80', 4.2, 6.0, 'Kiwi Express Shoe Po', '', '', '0000-00-00', '2019-07-10', 4, 2, 7, 50),
(36, 120, '96', 5.6, 8.0, 'For Kids Girls Brief', '', '', '0000-00-00', '2019-07-10', 4, 2, 9, 200),
(37, 120, '96', 139.3, 199.0, 'Optus 4g Oppo Ax5s S', '', '', '0000-00-00', '2019-07-10', 5, 3, 219, 50),
(38, 120, '96', 104.3, 149.0, 'Vodafone Smart N10 4', '', '', '0000-00-00', '2019-10-10', 5, 3, 159, 50),
(39, 120, '96', 69.3, 99.0, 'Optus 4g Optus X Spi', '', '', '0000-00-00', '2019-10-10', 5, 3, 89, 50),
(40, 120, '96', 28.0, 40.0, 'Energizer Car Kit Li', '', '', '0000-00-00', '2019-10-10', 5, 3, 35, 60),
(41, 120, '96', 24.5, 35.0, 'Energizer Type C Wal', '', '', '0000-00-00', '2019-06-10', 5, 3, 27, 150),
(42, 120, '96', 21.0, 30.0, 'Energizer Cable Ligh', '', '', '0000-00-00', '2019-06-10', 5, 3, 25, 100),
(71, 100, '100', 5.0, 10.0, 'coconut chocolate', 'delicious coconut chocolate from starbucks', 'cocochoc.jpg', '2019-12-31', '2019-12-01', 23, 6, 8.6, 50),
(72, 20, '20', 7.0, 10.0, 'starbucks coffee', 'special coffee from starbucks', 'coffee11.jpg', '2019-12-31', '2019-12-01', 21, 7, 9.2, 50),
(73, 10, '10', 10.0, 15.0, 'strawberry drink', 'special strawberry flavour drink from starbucks', 'drink111.jpg', '2019-12-26', '2019-12-01', 22, 8, 12, 50);

--
-- Dumping data for table `offer`
--

INSERT INTO `offer` (`offerid`, `forbranch`, `forcat`, `foritem`, `bid`, `itemid`, `catid`, `offername`, `offerdetails`, `offerposter`, `isdiscount`, `discount percentage`) VALUES
(1, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, 1, '10'),
(2, NULL, NULL, NULL, NULL, 2, NULL, NULL, NULL, NULL, 1, '20'),
(3, NULL, NULL, NULL, NULL, 5, NULL, NULL, NULL, NULL, 1, '30'),
(4, NULL, NULL, NULL, NULL, 8, NULL, NULL, NULL, NULL, 1, '10'),
(5, NULL, NULL, NULL, NULL, 9, NULL, NULL, NULL, NULL, 1, '5'),
(6, NULL, NULL, NULL, NULL, 12, NULL, NULL, NULL, NULL, 1, '5'),
(7, NULL, NULL, NULL, NULL, 14, NULL, NULL, NULL, NULL, 0, '10'),
(8, NULL, NULL, NULL, NULL, 16, NULL, NULL, NULL, NULL, 1, '5'),
(9, NULL, NULL, NULL, NULL, 18, NULL, NULL, NULL, NULL, 0, '15'),
(10, NULL, NULL, NULL, NULL, 19, NULL, NULL, NULL, NULL, 1, '20');

--
-- Dumping data for table `oreview`
--

INSERT INTO `oreview` (`cid`, `oid`, `comment`) VALUES
(3, 3, 'creative people'),
(5, 1, 'great organisation'),
(5, 3, 'all nice products '),
(6, 4, 'good work and great response'),
(7, 2, 'nice job'),
(7, 5, 'fantastic work and awesome products'),
(9, 4, 'good products'),
(10, 4, 'average');

--
-- Dumping data for table `organization`
--

INSERT INTO `organization` (`oid`, `name`, `logo`, `themecolor`, `contactno1`, `contactno2`, `headquarter`, `about`, `mustreg`, `canreg`, `mustapprove`) VALUES
(1, 'clothify', 'logos/l3.jpg', 'red', '9987656754', '9908765431', '5789', 'Shopclues is the latest addition to the top e-commerce websites in India. Unlike Amazon and Flipkart, Shopclues is a market place that focuses on unstructured categories of home, electrical, fashion, and daily utility items.  The mass market of shopclues comes from tier 2 and tier 3 cities and most of its business comes from smaller cities. Shopclues helps give brands from unstructured markets a voice of its own.', 1, 1, 1),
(2, 'myecommerce', 'logos/l4.jpg', 'black', '7456789234', '7865481234', '2785', 'PayTm is the second largest e-commerce platform in India and has also made its way to the list of unicorn startups. Primarily started as a mobile wallet, in 2016, PayTm entered the e-commerce industry with PayTm Mall. As the name suggests, it is an online market place for products ranging from electronics to daily consumer needs.', 1, 1, 1),
(3, 'micromerchant', 'logos/l5.jpg', 'grey', '7890654321', '8907654323', '4352', 'IT has a comparatively larger merchant base. It focuses on small and medium sized traders located in smaller cities and helps them take their business online. With over 50 million visitors on its website, one of the major revenue generating categories has been the home and kitchen appliances category', 1, 1, 1),
(4, 'starbie', 'starbie.png', 'red', '452597785', '452103658', '1', 'The given company is StarBie.', 1, 1, 1),
(5, 'woolworths', 'logos/woolworths.png', 'greenyellow', '0403040404', '0304034003', 'Brisbane', 'This is the fresh food people. This is the fresh food people. This is the fresh food people. This is the fresh food people. This is the fresh food people. This is the fresh food people. This is the fresh food people. This is the fresh food people. This is the fresh food people. This is the fresh food people. This is the fresh food people. This is the fresh food people. This is the fresh food people. This is the fresh food people. This is the fresh food people.', 1, 1, 0),
(6, 'subway', 'logos/subway.png', '', '123 123 123', '222 333 444', 'Brisbane', 'Subway is an American privately-held restaurant franchise that primarily sells submarine sandwiches and salads. It is one of the fastest-growing franchises in the world and, as of October 2019, had 41,512 locations in more than 100 countries. More than half its locations are in the United States', 0, 1, 1),
(7, 'starbucks', 'logos/starbucks.png', 'green', '666 666 666', '555 555 555', 'Brisbane', 'Starbucks Corporation is an American coffee company and coffeehouse chain. Starbucks was founded in Seattle, Washington in 1971. As of early 2019, the company operates over 30,000 locations worldwide', 0, 0, 0);

--
-- Dumping data for table `review`
--

INSERT INTO `review` (`cid`, `tid`, `itemid`, `comment`) VALUES
(3, 1, 76, 'great product'),
(5, 6, 10, 'satisfy with the product'),
(7, 4, 25, 'good response'),
(9, 3, 34, 'nice work'),
(10, 2, 34, 'fast delivery');

--
-- Dumping data for table `transaction`
--

INSERT INTO `transaction` (`tid`, `date`, `totalprice`, `cid`, `paid`) VALUES
(1, '2019-10-12', 800, 1, 1),
(2, '2019-10-13', 358, 2, 1),
(3, '2019-10-14', 973, 3, 0),
(4, '2019-10-15', 400, 4, 0),
(5, '2019-11-16', 480, 1, 1),
(6, '2019-11-17', 520, 1, 1),
(7, '2019-11-18', 1230, 3, 0),
(8, '2019-11-19', 298, 5, 1),
(9, '2019-11-20', 510, 6, 1);

--
-- Dumping data for table `transactionitem`
--

INSERT INTO `transactionitem` (`itemid`, `tid`, `amount`, `totalprice`) VALUES
(1, 1, 10, '10'),
(1, 8, 24, '24'),
(2, 1, 20, '20'),
(2, 2, 22, '22'),
(2, 8, 25, '25'),
(3, 1, 30, '30'),
(3, 2, 2, '2'),
(3, 8, 26, '26'),
(4, 1, 20, '20'),
(4, 8, 27, '27'),
(5, 2, 30, '210'),
(5, 5, 10, '70'),
(5, 8, 28, '196'),
(6, 1, 40, '360'),
(6, 5, 10, '90'),
(7, 5, 10, '80'),
(8, 5, 10, '100'),
(11, 3, 23, '115'),
(12, 3, 33, '132'),
(13, 3, 44, '176'),
(14, 3, 11, '44'),
(14, 5, 10, '40'),
(15, 3, 33, '330'),
(15, 5, 10, '100'),
(16, 3, 44, '132'),
(17, 3, 11, '44'),
(20, 4, 4, '200'),
(21, 6, 20, '40'),
(22, 6, 20, '140'),
(22, 9, 10, '70'),
(23, 6, 20, '100'),
(24, 6, 20, '120'),
(25, 6, 20, '60'),
(26, 6, 20, '60'),
(27, 9, 20, '80'),
(29, 9, 30, '120'),
(30, 9, 40, '240'),
(31, 7, 30, '270'),
(32, 2, 31, '124'),
(32, 7, 30, '120'),
(33, 7, 30, '120'),
(34, 1, 20, '360'),
(34, 7, 30, '540'),
(35, 7, 30, '180'),
(40, 4, 1, '40'),
(41, 4, 2, '70'),
(42, 4, 3, '90');

COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
