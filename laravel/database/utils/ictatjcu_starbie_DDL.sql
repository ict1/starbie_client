-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Dec 19, 2019 at 10:05 AM
-- Server version: 5.6.40-84.0-log
-- PHP Version: 7.3.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ictatjcu_starbie`
--

-- --------------------------------------------------------

--
-- Table structure for table `address`
--

CREATE TABLE `address` (
  `adid` int(11) NOT NULL,
  `streetname` varchar(20) DEFAULT NULL,
  `postcode` varchar(4) DEFAULT NULL,
  `log` decimal(10,0) DEFAULT NULL,
  `lat` decimal(10,0) DEFAULT NULL,
  `country` varchar(20) DEFAULT NULL,
  `city` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `aid` int(11) NOT NULL,
  `fname` varchar(20) NOT NULL,
  `mnames` varchar(50) NOT NULL,
  `lname` varchar(20) NOT NULL,
  `dob` date NOT NULL,
  `gender` varchar(1) NOT NULL,
  `nationality` varchar(30) DEFAULT NULL,
  `profilepicture` varchar(100) DEFAULT NULL,
  `jobposition` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `street_num` varchar(10) NOT NULL,
  `postaladdress` int(11) DEFAULT NULL,
  `oid` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `branches`
--

CREATE TABLE `branches` (
  `bid` int(11) NOT NULL,
  `oid` int(11) NOT NULL,
  `street_num` varchar(10) NOT NULL,
  `postaladdressid` int(11) DEFAULT NULL,
  `pobox` varchar(6) DEFAULT NULL,
  `contactno` varchar(20) NOT NULL,
  `about` varchar(2000) DEFAULT NULL,
  `gallery` varchar(100) DEFAULT NULL,
  `addressid` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `catid` int(11) NOT NULL,
  `iid` int(11) NOT NULL COMMENT 'Inventory ID',
  `name` varchar(20) NOT NULL,
  `description` varchar(2000) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `contact`
--

CREATE TABLE `contact` (
  `contid` int(15) NOT NULL,
  `name` varchar(30) NOT NULL,
  `email` varchar(30) NOT NULL,
  `message` varchar(200) NOT NULL,
  `oid` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `customer`
--

CREATE TABLE `customer` (
  `cid` int(11) NOT NULL,
  `fname` varchar(20) NOT NULL,
  `mnames` varchar(50) DEFAULT NULL,
  `lname` varchar(20) NOT NULL,
  `dob` date NOT NULL,
  `gender` varchar(1) DEFAULT NULL,
  `addressid` int(11) DEFAULT NULL,
  `paddressid` int(11) DEFAULT NULL,
  `password` varchar(50) DEFAULT NULL,
  `approved` int(11) DEFAULT NULL COMMENT '0 or 1',
  `session` int(11) DEFAULT NULL COMMENT '0 or 1',
  `profilepic` varchar(100) DEFAULT NULL,
  `email` varchar(100) NOT NULL,
  `oid` int(11) NOT NULL,
  `streetnumber` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `field`
--

CREATE TABLE `field` (
  `fieldid` int(11) NOT NULL,
  `forbranch` int(11) DEFAULT NULL COMMENT '0 or 1',
  `forcust` int(11) DEFAULT NULL COMMENT '0 or 1',
  `forinvitem` int(11) DEFAULT NULL COMMENT '0 or 1',
  `fieldname` varchar(20) DEFAULT NULL,
  `fieldtype` varchar(20) DEFAULT NULL,
  `oid` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `fieldfilled`
--

CREATE TABLE `fieldfilled` (
  `fieldid` int(11) NOT NULL,
  `bid` int(11) NOT NULL,
  `itemid` int(11) NOT NULL,
  `cid` int(11) NOT NULL,
  `information` varchar(1000) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `inventory`
--

CREATE TABLE `inventory` (
  `iid` int(11) NOT NULL,
  `bid` int(11) DEFAULT NULL,
  `addressid` int(11) DEFAULT NULL,
  `pobox` varchar(6) DEFAULT NULL,
  `contactno` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `inventoryitem`
--

CREATE TABLE `inventoryitem` (
  `itemid` int(11) NOT NULL,
  `amountexisting` int(11) NOT NULL,
  `amountshown` decimal(10,0) DEFAULT NULL COMMENT 'can be percentage in decimal or a cieling number (for now a 1 is a 100% and not 1 item)',
  `pricebought` float(10,1) NOT NULL,
  `priceselling` float(10,1) DEFAULT NULL,
  `name` varchar(20) NOT NULL,
  `description` varchar(2000) DEFAULT NULL,
  `gallery` varchar(100) NOT NULL,
  `expirydate` date DEFAULT NULL,
  `datebought` date NOT NULL,
  `catid` int(11) DEFAULT NULL COMMENT 'for category',
  `iid` int(11) NOT NULL COMMENT 'inventory foreign key',
  `CompetitorPrice` float DEFAULT NULL,
  `MinimumStockLevel` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `offer`
--

CREATE TABLE `offer` (
  `offerid` int(11) NOT NULL,
  `forbranch` int(11) DEFAULT NULL,
  `forcat` int(11) DEFAULT NULL,
  `foritem` int(11) DEFAULT NULL,
  `bid` int(11) DEFAULT NULL,
  `itemid` int(11) DEFAULT NULL,
  `catid` int(11) DEFAULT NULL,
  `offername` varchar(20) DEFAULT NULL,
  `offerdetails` varchar(2000) DEFAULT NULL,
  `offerposter` varchar(100) DEFAULT NULL,
  `isdiscount` int(11) NOT NULL COMMENT '0 or 1',
  `discount percentage` decimal(10,0) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `oreview`
--

CREATE TABLE `oreview` (
  `cid` int(11) NOT NULL,
  `oid` int(11) NOT NULL,
  `comment` varchar(1000) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `organization`
--

CREATE TABLE `organization` (
  `oid` int(11) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `logo` varchar(100) DEFAULT NULL,
  `themecolor` varchar(20) NOT NULL,
  `contactno1` varchar(20) DEFAULT NULL,
  `contactno2` varchar(20) DEFAULT NULL,
  `headquarter` varchar(11) DEFAULT NULL,
  `about` varchar(2000) NOT NULL,
  `mustreg` int(11) DEFAULT NULL COMMENT '0 or 1',
  `canreg` int(11) DEFAULT NULL COMMENT '0 or 1',
  `mustapprove` int(11) DEFAULT NULL COMMENT '0 or 1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `review`
--

CREATE TABLE `review` (
  `cid` int(11) NOT NULL,
  `tid` int(11) NOT NULL COMMENT 'FK transaction item',
  `itemid` int(11) NOT NULL COMMENT 'FK transaction item',
  `comment` varchar(1000) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `save_reports`
--

CREATE TABLE `save_reports` (
  `id` int(10) NOT NULL,
  `save_name` varchar(200) NOT NULL,
  `save_url` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `transaction`
--

CREATE TABLE `transaction` (
  `tid` int(11) NOT NULL,
  `date` date NOT NULL,
  `totalprice` int(11) DEFAULT NULL,
  `cid` int(11) NOT NULL COMMENT 'customer id',
  `paid` int(11) NOT NULL COMMENT '0 or 1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `transactionitem`
--

CREATE TABLE `transactionitem` (
  `itemid` int(11) NOT NULL,
  `tid` int(11) NOT NULL,
  `amount` int(11) NOT NULL,
  `totalprice` decimal(10,0) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `address`
--
ALTER TABLE `address`
  ADD PRIMARY KEY (`adid`);

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`aid`),
  ADD KEY `oid` (`oid`),
  ADD KEY `postaladdress` (`postaladdress`);

--
-- Indexes for table `branches`
--
ALTER TABLE `branches`
  ADD PRIMARY KEY (`bid`),
  ADD KEY `oid` (`oid`),
  ADD KEY `postaladdressid` (`postaladdressid`);

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`catid`),
  ADD KEY `iid` (`iid`);

--
-- Indexes for table `contact`
--
ALTER TABLE `contact`
  ADD PRIMARY KEY (`contid`);

--
-- Indexes for table `customer`
--
ALTER TABLE `customer`
  ADD PRIMARY KEY (`cid`),
  ADD KEY `addressid` (`addressid`),
  ADD KEY `paddressid` (`paddressid`);

--
-- Indexes for table `field`
--
ALTER TABLE `field`
  ADD PRIMARY KEY (`fieldid`);

--
-- Indexes for table `fieldfilled`
--
ALTER TABLE `fieldfilled`
  ADD KEY `fieldid` (`fieldid`),
  ADD KEY `bid` (`bid`),
  ADD KEY `itemid` (`itemid`),
  ADD KEY `cid` (`cid`);

--
-- Indexes for table `inventory`
--
ALTER TABLE `inventory`
  ADD PRIMARY KEY (`iid`),
  ADD KEY `bid` (`bid`),
  ADD KEY `addressid` (`addressid`);

--
-- Indexes for table `inventoryitem`
--
ALTER TABLE `inventoryitem`
  ADD PRIMARY KEY (`itemid`),
  ADD KEY `catid` (`catid`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `offer`
--
ALTER TABLE `offer`
  ADD PRIMARY KEY (`offerid`),
  ADD KEY `bid` (`bid`),
  ADD KEY `itemid` (`itemid`),
  ADD KEY `catid` (`catid`);

--
-- Indexes for table `oreview`
--
ALTER TABLE `oreview`
  ADD PRIMARY KEY (`cid`,`oid`),
  ADD KEY `cid` (`cid`),
  ADD KEY `oid` (`oid`);

--
-- Indexes for table `organization`
--
ALTER TABLE `organization`
  ADD PRIMARY KEY (`oid`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`(191));

--
-- Indexes for table `review`
--
ALTER TABLE `review`
  ADD PRIMARY KEY (`cid`,`tid`,`itemid`),
  ADD KEY `cid` (`cid`),
  ADD KEY `tid` (`tid`),
  ADD KEY `itemid` (`itemid`);

--
-- Indexes for table `save_reports`
--
ALTER TABLE `save_reports`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `transaction`
--
ALTER TABLE `transaction`
  ADD PRIMARY KEY (`tid`),
  ADD KEY `cid` (`cid`);

--
-- Indexes for table `transactionitem`
--
ALTER TABLE `transactionitem`
  ADD PRIMARY KEY (`itemid`,`tid`),
  ADD KEY `tid` (`tid`),
  ADD KEY `itemid` (`itemid`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `address`
--
ALTER TABLE `address`
  MODIFY `adid` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `aid` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `branches`
--
ALTER TABLE `branches`
  MODIFY `bid` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `catid` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `contact`
--
ALTER TABLE `contact`
  MODIFY `contid` int(15) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `customer`
--
ALTER TABLE `customer`
  MODIFY `cid` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `field`
--
ALTER TABLE `field`
  MODIFY `fieldid` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `inventory`
--
ALTER TABLE `inventory`
  MODIFY `iid` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `inventoryitem`
--
ALTER TABLE `inventoryitem`
  MODIFY `itemid` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `offer`
--
ALTER TABLE `offer`
  MODIFY `offerid` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `organization`
--
ALTER TABLE `organization`
  MODIFY `oid` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `save_reports`
--
ALTER TABLE `save_reports`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `transaction`
--
ALTER TABLE `transaction`
  MODIFY `tid` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
