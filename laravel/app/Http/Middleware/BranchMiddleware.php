<?php

namespace App\Http\Middleware;

use Closure;
use App\Starbie\Branches\Branch;
use App\Http\View\Composers\BranchComposer;
use Illuminate\View\View;

class BranchMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        session('branch', session('org')->branches()->first());
        return $next($request);
    }
}
