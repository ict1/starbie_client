<?php

namespace App\Http\View\Composers;

use Illuminate\View\View;
use Config;
use App\Starbie\Offers\Offer;

class OfferComposer
{
    private $offers;

    public function __construct()
    {
        $branch = app()->get(BranchComposer::class)->branch();
        $this->offers = Offer::where('foritem', 1)->where('bid', $branch->bid)->get();
    }

    public function compose(View $view)
    {   
        $view->with(
            'offers', session('offers', $this->offers));
    }

}