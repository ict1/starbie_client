<?php

namespace App\Http\View\Composers;

use Illuminate\View\View;
use Config;

class BranchComposer
{
    private $branches;
    private $branch;

    public function __construct()
    {
        $org = session('org');
        $this->branches = $org->branches()->get();
        $this->branch = $this->branches->first();
    }

    public function compose(View $view)
    {   
        $view->with(
            'branch', session('branch', $this->branch))
                ->with(
                    'branches', $this->branches);
    }

    public function branch()
    {
        return session('branch', $this->branch);
    }
}