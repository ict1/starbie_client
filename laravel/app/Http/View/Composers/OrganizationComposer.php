<?php

namespace App\Http\View\Composers;

use Illuminate\View\View;
use Illuminate\Http\Request;
use App\Starbie\Organizations\Organization;
use Config;

class OrganizationComposer
{
    public function __construct()
    {
        $oid = config('organization.oid');
        session(['org' => Organization::where('oid', $oid)->first()]);
    }

    public function compose(View $view)
    {   
        $view->with('org', session('org'));
    }
}