<?php

namespace App\Http\View\Composers;

use Illuminate\View\View;
use Config;

class CategoryComposer
{
    private $categories;

    public function __construct()
    {
        
    }

    public function compose(View $view)
    {   
        $branch = app()->get(BranchComposer::class)->branch();
        $this->categories = ($branch->inventory()->first())->categories()->get();     
        $view->with('categories', session('categories', $this->categories));
    }
}