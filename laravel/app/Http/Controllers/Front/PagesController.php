<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Container;
use Illuminate\View\View;

class PagesController extends Controller
{

    public function index()    
    {
        return view('front.index');
    }

    public function TNC()
    {
        return view('front.TNC');
    }

}