<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AccountController extends Controller
{
    public function index()
    {
        return view('front.account')
            ->with('customer', auth()->user())
            ->with('orders', auth()->user()->transactions()->get())
            ->with('address', auth()->user()->address()->get());
    }
}