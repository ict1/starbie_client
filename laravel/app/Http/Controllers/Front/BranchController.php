<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\View\Composers\BranchComposer;
use App\Branch;
use Illuminate\Support\Facades\View;
use App\Http\Middleware\BranchMiddleware;

class BranchController extends Controller
{
    public function index(Request $request)
    {
        session(['branch' => Branch::where('bid', $request->branch_id)->first()]);
        return view('front.index');
    }

    public function about()    
    {
        return view('front.about');
    }

    public function contact()    
    {
        return view('front.contact');
    }

    public function send(Request $request)
    {
        $request->session()->flash('message', "Thank you for contacting us. We will get back to you soon.");
        return view('front.contact');
    }

}
