<?php

namespace App\Http\Controllers\Front;

use App\Starbie\InventoryItems\InventoryItem;
use App\Starbie\Categories\Category;
use App\Http\Controllers\Controller;
use App\Starbie\Organizations\Organization;
use Illuminate\Http\Request;
use App\Starbie\Reviews\Review;
use App\Starbie\Transactions\TransactionItem;
use App\Starbie\Transactions\Transaction;

class InventoryItemController extends Controller
{

    /**
     * 
     * @return Response
     */
    public function search(Request $request)    
    {
        $term = $request->get('term');
        $items = null;
        if ($term == '')
        {
            $term = '%';
        }
        $category = Category::where('name', 'LIKE', '%'.$term.'%')->first();
        $item = InventoryItem::where('name', 'LIKE', '%'.$term.'%')->first();
        if ($category != null)
        {
            $items = InventoryItem::where('catid', $category->catid)->get();
        }
        else if ($item != null)
        {
            $items = InventoryItem::where('catid', $item->catid)->get();
        }

        $organization = Organization::where('oid', env('ORGANIZATION_ID'))->first();
            
        return view('product.search', ['organization' => $organization, 'products' => $items]);

    }

    public function show($product_id)
    {    
        $item = InventoryItem::where('itemid', $product_id)->first();
        if ($item != null)
        {
            return view('product.show')->with('product', $item);
        }
        return view('front.index');
    }

    public function addComment(Request $request)
    {
        $review = new Review;
        $review->comment = $request->comment;
        $review->itemid = $request->itemid;
        $review->cid = auth()->user()->cid; 
        $transitem = TransactionItem::where('itemid', $review->itemid)->first();
        $error = FALSE;
        if ($transitem != NULL)
        {
            $tidquery = Transaction::where('cid', $review->cid)->where('tid', $transitem->tid)->first();
            if ($tidquery != NULL)
            {
                $review->tid = $tidquery->tid;
                $review->save();
            }
            else
            {
                $error = TRUE;
            }
        }
        else
        {
            $error = TRUE;
        }
        if ($error)
        {
            $request->session()->flash('error', "You must buy this item to be able to add a review.");
        }
        return redirect()->back();
    }

}
