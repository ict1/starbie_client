<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Starbie\Organizations\Organization;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use App\Http\View\Composers\OrganizationComposer;

class OrganizationController extends Controller
{
    public function reviews()    
    {
        return view('front.oreviews')->with('reviews', session('org')->oreviews()->get());
    }
}
