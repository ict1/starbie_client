<?php

namespace App\Http\Controllers\Front;

use App\Starbie\InventoryItems\InventoryItem;
use App\Starbie\Categories\Category;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class SearchBarController extends Controller
{

    /**
     * 
     * @return Response
     */
    public function search(Request $request)    
    {
        $term = $request->get('term');
        $items = null;
        if (strlen($term) == 0)
        {
            $term = '%';
        }
        $catid = $request->get("category_id");
        if ($catid != 0) {
            $category = Category::where('catid', $catid)->first();
        }
        else {
            $category = Category::where('name', 'LIKE', '%'.$term.'%')->first();
        }
        $item = InventoryItem::where('name', 'LIKE', '%'.$term.'%')->first();
        if ($category != null)
        {
            $items = InventoryItem::where('catid', $category->catid)->get();
        }
        else if ($item != null)
        {
            $items = InventoryItem::where('catid', $item->catid)->get();
        }
            
        return view('product.search')->with('products', $items);

    }

    public function show(Request $request)
    {    
        $item = InventoryItem::where('itemid', $request->input('id', false))->first();
        if ($item != null)
        {
            return view('product.show')->with('product', $item);
        }
        return view('front.home');
    }

}
