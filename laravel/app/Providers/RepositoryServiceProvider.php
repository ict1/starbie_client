<?php

namespace App\Providers;

use App\Starbie\Addresses\Repositories\AddressRepository;
use App\Starbie\Addresses\Repositories\Interfaces\AddressRepositoryInterface;
use App\Starbie\Carts\Repositories\CartRepository;
use App\Starbie\Carts\Repositories\Interfaces\CartRepositoryInterface;
use App\Starbie\Categories\Repositories\CategoryRepository;
use App\Starbie\Categories\Repositories\Interfaces\CategoryRepositoryInterface;
use App\Starbie\Customers\Repositories\CustomerRepository;
use App\Starbie\Customers\Repositories\Interfaces\CustomerRepositoryInterface;
use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->bind(
            CustomerRepositoryInterface::class,
            CustomerRepository::class
        );

        $this->app->bind(
            CategoryRepositoryInterface::class,
            CategoryRepository::class
        );

        $this->app->bind(
            AddressRepositoryInterface::class,
            AddressRepository::class
        );

        $this->app->bind(
            CartRepositoryInterface::class,
            CartRepository::class
        );

    }
}
