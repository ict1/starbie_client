<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Http\Request;
use Config;
use Illuminate\Support\Facades\View;
use Schema;
use App\Starbie\Organizations\Organization;
use App\Http\View\Composers\OrganizationComposer;
use App\Http\View\Composers\BranchComposer;
use App\Http\View\Composers\CategoryComposer;
use App\Http\View\Composers\OfferComposer;

class SettingsServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {   
        $this->app->singleton(OrganizationComposer::class);
        $this->app->singleton(BranchComposer::class);
        $this->app->singleton(BranchMiddleware::class);
        $this->app->singleton(CategoryComposer::class);
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        // Initiatialize organizations, with respective branches and inventories from database
        if (Schema::hasTable('organization'))
        {
            View::composer('front.index', function($view){
                $org_names = Config::get('globalsettings.organizations');
                $org_instances = Organization::findByManyNames($org_names);
                $view->with('organizations', $org_instances);
            });
        }

        View::composer(['*', ], OrganizationComposer::class);
        View::composer(['*', ], BranchComposer::class);
        View::composer(['*', ], CategoryComposer::class);
        View::composer(['*', ], OfferComposer::class);
    }
}
