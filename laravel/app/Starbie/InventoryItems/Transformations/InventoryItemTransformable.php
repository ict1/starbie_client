<?php

namespace App\Starbie\InventoryItems\Transformations;

use App\Starbie\InventoryItems\InventoryItem;
use Illuminate\Support\Facades\Storage;

trait InventoryItemTransformable
{
    /**
     * Transform the product
     *
     * @param InventoryItem $product
     * @return InventoryItem
     */
    protected function transformInventoryItem(InventoryItem $product)
    {
        $prod = new InventoryItem;
        $prod->id = (int) $product->id;
        $prod->name = $product->name;
        $prod->sku = $product->sku;
        $prod->slug = $product->slug;
        $prod->description = $product->description;
        $prod->cover = asset("storage/$product->cover");
        $prod->quantity = $product->quantity;
        $prod->price = $product->price;
        $prod->status = $product->status;
        $prod->weight = (float) $product->weight;
        $prod->mass_unit = $product->mass_unit;
        $prod->sale_price = $product->sale_price;
        $prod->brand_id = (int) $product->brand_id;

        return $prod;
    }
}
