<?php

namespace App\Starbie\InventoryItems\Exceptions;

class InventoryItemInvalidArgumentException extends \Exception
{
}
