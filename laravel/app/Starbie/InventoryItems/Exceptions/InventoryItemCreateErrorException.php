<?php

namespace App\Starbie\InventoryItems\Exceptions;

class InventoryItemCreateErrorException extends \Exception
{
}
