<?php

namespace App\Starbie\Products\Exceptions;

class ProductNotFoundException extends \Exception
{
}
