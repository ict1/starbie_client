<?php

namespace App\Starbie\InventoryItems\Exceptions;

class InventoryItemUpdateErrorException extends \Exception
{
}
