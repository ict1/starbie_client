<?php

namespace App\Starbie\InventoryItems\Requests;

use App\Starbie\Base\BaseFormRequest;
use Illuminate\Validation\Rule;
    
class UpdateInventoryItemRequest extends BaseFormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'sku' => ['required'],
            'name' => ['required', Rule::unique('products')->ignore($this->segment(3))],
            'quantity' => ['required', 'integer'],
            'price' => ['required']
        ];
    }
}
