<?php

namespace App\Starbie\InventoryItems;

use Illuminate\Database\Eloquent\Model;
use App\Starbie\Inventories\Inventory;
use App\Starbie\Categories\Category;
use App\Starbie\Reviews\Review;
use App\Starbie\Offers\Offer;

class InventoryItem extends Model
{
    protected $table = 'inventoryitem';
    protected $primaryKey = "itemid";

    public function inventory()
    {
        return $this->belongsTo(Inventory::class, 'iid');
    }

    public function category()
    {
        return $this->belongsTo(Category::class, 'catid');
    }

    public function reviews()
    {
        return $this->hasMany(Review::class, 'itemid');
    }

    public function offers()
    {
        return $this->hasMany(Offer::class, 'itemid');
    }
    
}
