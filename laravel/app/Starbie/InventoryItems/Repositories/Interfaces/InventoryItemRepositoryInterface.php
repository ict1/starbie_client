<?php

namespace App\Starbie\InventoryItems\Repositories\Interfaces;

use App\Starbie\AttributeValues\AttributeValue;
use Jsdecena\Baserepo\BaseRepositoryInterface;
use App\Starbie\Brands\Brand;
use App\Starbie\InventoryItemAttributes\InventoryItemAttribute;
use App\Starbie\InventoryItems\InventoryItem;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Collection;

interface InventoryItemRepositoryInterface extends BaseRepositoryInterface
{
    public function listInventoryItems(string $order = 'id', string $sort = 'desc', array $columns = ['*']) : Collection;

    public function createInventoryItem(array $data) : InventoryItem;

    public function updateInventoryItem(array $data) : bool;

    public function findInventoryItemById(int $id) : InventoryItem;

    public function deleteInventoryItem(InventoryItem $product) : bool;

    public function removeInventoryItem() : bool;

    public function detachCategories();

    public function getCategories() : Collection;

    public function syncCategories(array $params);

    public function deleteFile(array $file, $disk = null) : bool;

    public function deleteThumb(string $src) : bool;

    public function findInventoryItemBySlug(array $slug) : InventoryItem;

    public function searchInventoryItem(string $text) : Collection;

    public function findInventoryItemImages() : Collection;

    public function saveCoverImage(UploadedFile $file) : string;

    public function saveInventoryItemImages(Collection $collection);

    public function saveInventoryItemAttributes(InventoryItemAttribute $productAttribute) : InventoryItemAttribute;

    public function listInventoryItemAttributes() : Collection;

    public function removeInventoryItemAttribute(InventoryItemAttribute $productAttribute) : ?bool;

    public function saveCombination(InventoryItemAttribute $productAttribute, AttributeValue ...$attributeValues) : Collection;

    public function listCombinations() : Collection;

    public function findInventoryItemCombination(InventoryItemAttribute $attribute);

    public function saveBrand(Brand $brand);

    public function findBrand();
}
