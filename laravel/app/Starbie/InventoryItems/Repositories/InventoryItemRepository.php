<?php

namespace App\Starbie\InventoryItems\Repositories;

use App\Starbie\AttributeValues\AttributeValue;
use App\Starbie\InventoryItems\Exceptions\InventoryItemCreateErrorException;
use App\Starbie\InventoryItems\Exceptions\InventoryItemUpdateErrorException;
use App\Starbie\Tools\UploadableTrait;
use Jsdecena\Baserepo\BaseRepository;
use App\Starbie\Brands\Brand;
use App\Starbie\InventoryItemAttributes\InventoryItemAttribute;
use App\Starbie\InventoryItemImages\InventoryItemImage;
use App\Starbie\InventoryItems\Exceptions\InventoryItemNotFoundException;
use App\Starbie\InventoryItems\InventoryItem;
use App\Starbie\InventoryItems\Repositories\Interfaces\InventoryItemRepositoryInterface;
use App\Starbie\InventoryItems\Transformations\InventoryItemTransformable;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\QueryException;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

class InventoryItemRepository extends BaseRepository implements InventoryItemRepositoryInterface
{
    use InventoryItemTransformable, UploadableTrait;

    /**
     * InventoryItemRepository constructor.
     * @param InventoryItem $product
     */
    public function __construct(InventoryItem $product)
    {
        parent::__construct($product);
        $this->model = $product;
    }

    /**
     * List all the products
     *
     * @param string $order
     * @param string $sort
     * @param array $columns
     * @return Collection
     */
    public function listInventoryItems(string $order = 'id', string $sort = 'desc', array $columns = ['*']) : Collection
    {
        return $this->all($columns, $order, $sort);
    }

    /**
     * Create the product
     *
     * @param array $data
     *
     * @return InventoryItem
     * @throws InventoryItemCreateErrorException
     */
    public function createInventoryItem(array $data) : InventoryItem
    {
        try {
            return $this->create($data);
        } catch (QueryException $e) {
            throw new InventoryItemCreateErrorException($e);
        }
    }

    /**
     * Update the product
     *
     * @param array $data
     *
     * @return bool
     * @throws InventoryItemUpdateErrorException
     */
    public function updateInventoryItem(array $data) : bool
    {
        $filtered = collect($data)->except('image')->all();

        try {
            return $this->model->where('id', $this->model->id)->update($filtered);
        } catch (QueryException $e) {
            throw new InventoryItemUpdateErrorException($e);
        }
    }

    /**
     * Find the product by ID
     *
     * @param int $id
     *
     * @return InventoryItem
     * @throws InventoryItemNotFoundException
     */
    public function findInventoryItemById(int $id) : InventoryItem
    {
        try {
            return $this->transformInventoryItem($this->findOneOrFail($id));
        } catch (ModelNotFoundException $e) {
            throw new InventoryItemNotFoundException($e);
        }
    }

    /**
     * Delete the product
     *
     * @param InventoryItem $product
     *
     * @return bool
     * @throws \Exception
     * @deprecated
     * @use removeInventoryItem
     */
    public function deleteInventoryItem(InventoryItem $product) : bool
    {
        $product->images()->delete();
        return $product->delete();
    }

    /**
     * @return bool
     * @throws \Exception
     */
    public function removeInventoryItem() : bool
    {
        return $this->model->where('id', $this->model->id)->delete();
    }

    /**
     * Detach the categories
     */
    public function detachCategories()
    {
        $this->model->categories()->detach();
    }

    /**
     * Return the categories which the product is associated with
     *
     * @return Collection
     */
    public function getCategories() : Collection
    {
        return $this->model->categories()->get();
    }

    /**
     * Sync the categories
     *
     * @param array $params
     */
    public function syncCategories(array $params)
    {
        $this->model->categories()->sync($params);
    }

    /**
     * @param $file
     * @param null $disk
     * @return bool
     */
    public function deleteFile(array $file, $disk = null) : bool
    {
        return $this->update(['cover' => null], $file['product']);
    }

    /**
     * @param string $src
     * @return bool
     */
    public function deleteThumb(string $src) : bool
    {
        return DB::table('product_images')->where('src', $src)->delete();
    }

    /**
     * Get the product via slug
     *
     * @param array $slug
     *
     * @return InventoryItem
     * @throws InventoryItemNotFoundException
     */
    public function findInventoryItemBySlug(array $slug) : InventoryItem
    {
        try {
            return $this->findOneByOrFail($slug);
        } catch (ModelNotFoundException $e) {
            throw new InventoryItemNotFoundException($e);
        }
    }

    /**
     * @param string $text
     * @return mixed
     */
    public function searchInventoryItem(string $text) : Collection
    {
        if (!empty($text)) {
            return $this->model->searchInventoryItem($text);
        } else {
            return $this->listInventoryItems();
        }
    }

    /**
     * @return mixed
     */
    public function findInventoryItemImages() : Collection
    {
        return $this->model->images()->get();
    }

    /**
     * @param UploadedFile $file
     * @return string
     */
    public function saveCoverImage(UploadedFile $file) : string
    {
        return $file->store('products', ['disk' => 'public']);
    }

    /**
     * @param Collection $collection
     *
     * @return void
     */
    public function saveInventoryItemImages(Collection $collection)
    {
        $collection->each(function (UploadedFile $file) {
            $filename = $this->storeFile($file);
            $productImage = new InventoryItemImage([
                'product_id' => $this->model->id,
                'src' => $filename
            ]);
            $this->model->images()->save($productImage);
        });
    }

    /**
     * Associate the product attribute to the product
     *
     * @param InventoryItemAttribute $productAttribute
     * @return InventoryItemAttribute
     */
    public function saveInventoryItemAttributes(InventoryItemAttribute $productAttribute) : InventoryItemAttribute
    {
        $this->model->attributes()->save($productAttribute);
        return $productAttribute;
    }

    /**
     * List all the product attributes associated with the product
     *
     * @return Collection
     */
    public function listInventoryItemAttributes() : Collection
    {
        return $this->model->attributes()->get();
    }

    /**
     * Delete the attribute from the product
     *
     * @param InventoryItemAttribute $productAttribute
     *
     * @return bool|null
     * @throws \Exception
     */
    public function removeInventoryItemAttribute(InventoryItemAttribute $productAttribute) : ?bool
    {
        return $productAttribute->delete();
    }

    /**
     * @param InventoryItemAttribute $productAttribute
     * @param AttributeValue ...$attributeValues
     *
     * @return Collection
     */
    public function saveCombination(InventoryItemAttribute $productAttribute, AttributeValue ...$attributeValues) : Collection
    {
        return collect($attributeValues)->each(function (AttributeValue $value) use ($productAttribute) {
            return $productAttribute->attributesValues()->save($value);
        });
    }

    /**
     * @return Collection
     */
    public function listCombinations() : Collection
    {
        return $this->model->attributes()->map(function (InventoryItemAttribute $productAttribute) {
            return $productAttribute->attributesValues;
        });
    }

    /**
     * @param InventoryItemAttribute $productAttribute
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function findInventoryItemCombination(InventoryItemAttribute $productAttribute)
    {
        $values = $productAttribute->attributesValues()->get();

        return $values->map(function (AttributeValue $attributeValue) {
            return $attributeValue;
        })->keyBy(function (AttributeValue $item) {
            return strtolower($item->attribute->name);
        })->transform(function (AttributeValue $value) {
            return $value->value;
        });
    }

    /**
     * @param Brand $brand
     */
    public function saveBrand(Brand $brand)
    {
        $this->model->brand()->associate($brand);
    }

    /**
     * @return Brand
     */
    public function findBrand()
    {
        return $this->model->brand;
    }
}
