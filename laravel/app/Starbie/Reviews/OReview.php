<?php

namespace App\Starbie\Reviews;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use App\Starbie\Organizations\Organization;

class OReview extends Model
{
    protected $table = "oreview";
    
    protected function setKeysForSaveQuery(Builder $query)
    {
        $query
            ->where('cid', '=', $this->getAttribute('cid'))
            ->where('oid', '=', $this->getAttribute('oid'));

        return $query;
    }

    public function organization()    
    {
        return $this->belongsTo(Organization::class, 'oid');
    }
}
