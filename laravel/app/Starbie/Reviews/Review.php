<?php

namespace App\Starbie\Reviews;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use App\Starbie\InventoryItems\InventoryItem;
use App\Starbie\Customers\Customer;
use App\Starbie\Transactions\Transaction;

class Review extends Model
{
    protected $table = 'review';

    public function intentoryitem()
    {
        return $this->belongsTo(InventoryItem::class, 'itemid');
    }

    public function customer()
    {
        return $this->belongsTo(Customer::class, 'cid');
    }

    public function transaction()
    {
        return $this->belongsTo(Transaction::class, 'tid');
    }
}
