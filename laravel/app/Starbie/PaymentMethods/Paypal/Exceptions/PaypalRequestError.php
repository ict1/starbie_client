<?php

namespace App\Starbie\PaymentMethods\Paypal\Exceptions;

use Doctrine\Instantiator\Exception\InvalidArgumentException;

class PaypalRequestError extends InvalidArgumentException
{
}
