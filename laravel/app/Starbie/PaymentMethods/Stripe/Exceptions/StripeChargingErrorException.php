<?php

namespace App\Starbie\PaymentMethods\Stripe\Exceptions;

class StripeChargingErrorException extends \Exception
{
}
