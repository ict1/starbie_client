<?php

namespace App\Starbie\Addresses;

use Illuminate\Database\Eloquent\Model;
use App\Starbie\Branches\Branch;
use App\Starbie\Customers\Customer;

class Address extends Model
{
    protected $table = "address";
    protected $primaryKey = "adid";

    public function branch()
    {
        return $this->hasMany(Branch::class, 'addressid');
    }

    public function customer()
    {
        return $this->hasMany(Customer::class, 'addressid');
    }
}
