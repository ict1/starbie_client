<?php

namespace App\Starbie\Addresses\Transformations;

use App\Starbie\Addresses\Address;
use App\Starbie\Cities\Repositories\CityRepository;
use App\Starbie\Countries\Repositories\CountryRepository;
use App\Starbie\Customers\Customer;
use App\Starbie\Customers\Repositories\CustomerRepository;
use App\Starbie\Provinces\Province;
use App\Starbie\Provinces\Repositories\ProvinceRepository;
use App\Starbie\Cities\City;
use App\Starbie\Countries\Country;

trait AddressTransformable
{
    /**
     * Transform the address
     *
     * @param Address $address
     *
     * @return Address
     * @throws \App\Starbie\Cities\Exceptions\CityNotFoundException
     * @throws \App\Starbie\Countries\Exceptions\CountryNotFoundException
     * @throws \App\Starbie\Customers\Exceptions\CustomerNotFoundException
     */
    public function transformAddress(Address $address)
    {
        $obj = new Address;
        $obj->id = $address->id;
        $obj->alias = $address->alias;
        $obj->address_1 = $address->address_1;
        $obj->address_2 = $address->address_2;
        $obj->zip = $address->zip;
        $obj->city = $address->city;

        if (isset($address->province_id)) {
            $provinceRepo = new ProvinceRepository(new Province);
            $province = $provinceRepo->findProvinceById($address->province_id);
            $obj->province = $province->name;
        }

        $countryRepo = new CountryRepository(new Country);
        $country = $countryRepo->findCountryById($address->country_id);
        $obj->country = $country->name;

        $customerRepo = new CustomerRepository(new Customer);
        $customer = $customerRepo->findCustomerById($address->customer_id);
        $obj->customer = $customer->name;
        $obj->status = $address->status;

        return $obj;
    }
}
