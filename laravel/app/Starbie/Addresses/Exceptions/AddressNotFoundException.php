<?php

namespace App\Starbie\Addresses\Exceptions;

class AddressNotFoundException extends \Exception
{
}
