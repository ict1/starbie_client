<?php

namespace App\Starbie\Offers;

use Illuminate\Database\Eloquent\Model;
use App\Starbie\InventoryItems\InventoryItem;

class Offer extends Model
{
    protected $table = "offer";
    protected $primaryKey = "offerid";

    public function inventoryitem()
    {
        return $this->belongsTo(InventoryItem::class, 'itemid');
    }
}
