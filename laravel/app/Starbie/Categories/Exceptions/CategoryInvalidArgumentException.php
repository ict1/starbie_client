<?php

namespace App\Starbie\Categories\Exceptions;

use Doctrine\Instantiator\Exception\InvalidArgumentException;

class CategoryInvalidArgumentException extends InvalidArgumentException
{
}
