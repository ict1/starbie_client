<?php

namespace App\Starbie\Categories\Exceptions;

class CategoryNotFoundException extends \Exception
{
}
