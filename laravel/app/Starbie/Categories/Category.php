<?php

namespace App\Starbie\Categories;

use Illuminate\Database\Eloquent\Model;
use App\Starbie\Inventories\Inventory;

class Category extends Model
{
    protected $table = 'category';
    protected $primaryKey = "catid";

    public function inventory()
    {
        return $this->belongsTo(Inventory::class, 'iid');
    }
}
