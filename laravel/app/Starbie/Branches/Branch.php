<?php

namespace App\Starbie\Branches;

use Illuminate\Database\Eloquent\Model;
use App\Starbie\Organizations\Organization;
use App\Starbie\Addresses\Address;
use App\Starbie\Inventories\Inventory;

class Branch extends Model
{
    protected $table = 'branches';
    protected $primaryKey = "bid";

    public function __construct()
    {
    }

    public function organization()
    {
        return $this->belongsTo(Organization::class, 'oid');        
    }

    public function address()
    {
        return $this->belongsTo(Address::class, 'addressid');
    }

    public function inventory()
    {
        return $this->hasOne(Inventory::class, 'bid');
    }
}
