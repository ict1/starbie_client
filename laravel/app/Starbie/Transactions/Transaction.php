<?php

namespace App\Starbie\Transactions;

use Illuminate\Database\Eloquent\Model;
use App\Starbie\Transactions\TransactionItem;
use App\Starbie\Customers\Customer;
use App\Starbie\Reviews\Review;

class Transaction extends Model
{
    protected $table = 'transaction';
    protected $primaryKey = "tid";

    public function __construct()
    {
        
    }

    public function customer()
    {
        return $this->belongsTo(Customer::class);        
    }

    public function transactionitem()
    {
        return $this->hasMany(TransactionItem::class, 'tid');
    }

    public function review()
    {
        return $this->hasMany(Review::class, 'tid');
    }
}
