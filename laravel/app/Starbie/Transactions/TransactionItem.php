<?php

namespace App\Starbie\Transactions;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use App\Starbie\Transactions\Transaction;
use App\Starbie\InventoryItems\InventoryItem;

class TransactionItem extends Model
{
    protected $table = "transactionitem";

    protected function setKeysForSaveQuery(Builder $query)
    {
        $query
            ->where('itemid', '=', $this->getAttribute('itemid'))
            ->where('tid', '=', $this->getAttribute('tid'));

        return $query;
    }

    public function transaction()
    {
        return $this->belongsTo(Transaction::class, 'tid');
    }

    public function inventoryitem()
    {
        return $this->belongsTo(InventoryItem::class, 'itemid');
    }
}
