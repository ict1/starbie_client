<?php

namespace App\Starbie\Organizations;

use Illuminate\Database\Eloquent\Model;
use App\Starbie\Branches\Branch;
use App\Starbie\Reviews\OReview;

class Organization extends Model
{
    protected $table = 'organization';
    protected $primaryKey = "oid";

    public function __construct()
    {
        
    }

    /**
     * Search for all organizations given an array of names.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public static function scopeFindOrderedByName($query, array $orgs)
    {
        return $query->orderBy('name')->whereIn('name', $orgs)->get();
    }

    public static function findByManyNames(array $orgs)
    {
        return Organization::findOrderedByName($orgs);
    }

    public function branches()
    {
        return $this->hasMany(Branch::class, 'oid');
    }

    public function oreviews()
    {
        return $this->hasMany(OReview::class, 'oid');
    }
}
