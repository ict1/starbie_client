<?php

namespace App\Starbie\Cart\Requests;

use App\Starbie\Base\BaseFormRequest;

/**
 * Class CartCheckoutRequest
 * @package App\Starbie\Cart\Requests
 * @codeCoverageIgnore
 */
class CartCheckoutRequest extends BaseFormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'billing_address' => ['required']
        ];
    }
}
