<?php

namespace App\Starbie\Carts\Requests;

interface CheckoutInterface
{
    public function rules();
}
