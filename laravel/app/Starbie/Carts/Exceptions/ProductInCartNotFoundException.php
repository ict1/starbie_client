<?php

namespace App\Starbie\Carts\Exceptions;

class ProductInCartNotFoundException extends \Exception
{
}
