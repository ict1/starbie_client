<?php

namespace App\Starbie\Customers\Transformations;

use App\Starbie\Customers\Customer;

trait CustomerTransformable
{
    protected function transformCustomer(Customer $customer)
    {
        $prop = new Customer;
        $prop->id = (int) $customer->cid;
        $prop->name = $customer->fname;
        $prop->email = $customer->email;

        return $prop;
    }
}
