<?php

namespace App\Starbie\Customers\Exceptions;

class CreateCustomerInvalidArgumentException extends \Exception
{
}
