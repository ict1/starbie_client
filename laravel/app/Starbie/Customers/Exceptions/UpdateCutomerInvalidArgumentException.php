<?php

namespace App\Starbie\Customers\Exceptions;

class UpdateCustomerInvalidArgumentException extends \Exception
{
}
