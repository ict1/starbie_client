<?php

namespace App\Starbie\Customers\Exceptions;

class CustomerPaymentChargingErrorException extends \Exception
{
}
