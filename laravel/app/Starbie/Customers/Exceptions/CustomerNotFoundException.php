<?php

namespace App\Starbie\Customers\Exceptions;

class CustomerNotFoundException extends \Exception
{
}
