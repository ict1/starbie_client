<?php

namespace App\Starbie\Customers\Requests;

use App\Starbie\Base\BaseFormRequest;

class CreateCustomerRequest extends BaseFormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'oid' => ['required'],
            'fname' => ['required'],
            'lname' => ['required'],
            'email' => ['required', 'email', 'unique:customer'],
            'password' => ['required', 'min:8']
        ];
    }
}
