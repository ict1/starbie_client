<?php

namespace App\Starbie\Customers\Requests;

use App\Starbie\Base\BaseFormRequest;

class RegisterCustomerRequest extends BaseFormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'oid' => 'required',
            'fname' => 'required|string|max:20',
            'lname' => 'required|string|max:20',
            'email' => 'required|string|email|max:255|unique:customer',
            'password' => 'required|string|min:6|confirmed',
        ];
    }
}
