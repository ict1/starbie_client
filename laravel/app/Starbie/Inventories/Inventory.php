<?php

namespace App\Starbie\Inventories;

use Illuminate\Database\Eloquent\Model;
use App\Starbie\Branches\Branch;
use App\Starbie\Categories\Category;

class Inventory extends Model
{
    protected $table = 'inventory';
    protected $primaryKey = 'iid';

    public function branch()
    {
        return $this->belongsTo(Branch::class, 'bid');
    }

    public function categories()
    {
        return $this->hasMany(Category::class, 'iid');
    }
}
