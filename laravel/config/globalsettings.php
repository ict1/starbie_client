<?php

return [
    'organizations' => ['woolworths', 'subway', 'starbucks'],
    'API_KEY' => env('MAPS_API_KEY'),
];