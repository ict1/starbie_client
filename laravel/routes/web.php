<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



/*
|----------------------------------------------------------------------------
| Front End Routes
|----------------------------------------------------------------------------
*/

//Authentication Routes
Route::namespace('Auth')->group(function () {
    // Login Routes...
    Route::get('login', 'LoginController@showLoginForm')->name('login');
    Route::post('login', 'LoginController@login');
    Route::post('logout', 'LoginController@logout')->name('logout');

    // Cart Login Routes
    Route::get('cart/login', 'CartLoginController@showLoginForm')->name('cart.login');
    Route::post('cart/login', 'CartLoginController@login')->name('cart.login');
    Route::get('logout', 'LoginController@logout');

    // Registration Routes...
    Route::get('register', 'RegisterController@showRegistrationForm')->name('register');
    Route::post('register', 'RegisterController@register');
    Route::get('reqreg', 'RegisterController@showRegRegForm')->name('reqreg');
    Route::post('reqreg', 'RegisterController@reqreg');

    // Password Reset Routes...
    Route::get('password/reset', 'ForgotPasswordController@showLinkRequestForm')->name('password.request');
    Route::post('password/email', 'ForgotPasswordController@sendResetLinkEmail')->name('password.email');
    Route::get('password/reset/{token}', 'ResetPasswordController@showResetForm');
    Route::post('password/reset', 'ResetPasswordController@reset');
});

//Front-end Routes
Route::namespace('Front')->group(function () {
    //Navbar Routes
    Route::get('/', 'PagesController@index')->name('index');
    Route::post('/', 'BranchController@index')->name('branch');
    Route::get('/about', 'BranchController@about')->name('about');
    Route::post('/contact', 'BranchController@send')->name('contact');
    Route::get('/contact', 'BranchController@contact')->name('contact');
    Route::get('/reviews', 'OrganizationController@reviews')->name('oreviews');

    //Footer Routes
    Route::get('TNC','PagesController@TNC')->name('TNC');

    //Search bar
    Route::post('/search', 'SearchBarController@search')->name('search');
    Route::get('/search', 'SearchBarController@search')->name('search');
    Route::get('/{product_id}', 'InventoryItemController@show')->name('product.show');

    Route::resource('cart', 'CartController');

    //Login Required Routes
    Route::group(['middleware' => ['auth', 'web']], function () {
        // Shopping Cart Routes
        //Route::get("search", 'InventoryItemController@search')->name('search.product');
        
        Route::get('account', 'AccountController@index')->name('account');
        Route::get('checkout', 'CheckoutController@index')->name('checkout.index');
        Route::post('checkout', 'CheckoutController@store')->name('checkout.store');
        Route::get('checkout/execute', 'CheckoutController@executePayPalPayment')->name('checkout.execute');
        Route::post('checkout/execute', 'CheckoutController@charge')->name('checkout.execute');
        Route::get('checkout/cancel', 'CheckoutController@cancel')->name('checkout.cancel');
        Route::get('checkout/success', 'CheckoutController@success')->name('checkout.success');
        Route::resource('customer.address', 'CustomerAddressController');
        Route::post("comment", 'InventoryItemController@addComment')->name('product.review');

        //Bank Transfer
        //Route::namespace('Payments')->group(function () {
            //Route::get('bank-transfer', 'BankTransferController@index')->name('bank-transfer.index');
            //Route::post('bank-transfer', 'BankTransferController@store')->name('bank-transfer.store');
        //});

        //Route::namespace('Addresses')->group(function () {
            //Route::resource('country.state', 'CountryStateController');
            //Route::resource('state.city', 'StateCityController');
        //});
    });
});

    
    

/*
Route::namespace('Front')->group(function () {
    
    Route::get('/', 'PagesController@index')->name('index');

    Route::get('/about', 'BranchController@about')->name('about')->middleware('branch');

    Route::get('/contact', 'BranchController@contact')->name('contact');

    Route::post('/', 'BranchController@index');

    //Product
    // Search
    Route::post('/search', 'InventoryItemController@search');\
    // Show
    Route::get('/product', 'InventoryItemController@show');
});
*/

/*
|----------------------------------------------------------------------------
| Back End Hacks
|----------------------------------------------------------------------------
*/

Route::get('/refresh', function() {
    $exitCode = Artisan::call('cache:clear');
    $exitCode = Artisan::call('config:cache');
    return redirect()->route('index');
});

