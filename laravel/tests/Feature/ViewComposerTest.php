<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use Illuminate\View\View;
use App\Organization;
use App\Http\View\Composers\OrganizationComposer;
use Mockery;

class ViewComposerTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function it_passes_organization_object_to_view()
    {
        $composer = new OrganizationComposer();
        $composer->register('5');

        $view = Mockery::mock(View::class);

        $view->shouldReceive('with')->with('org', Organization::class)->once();

        $composer->compose($view);
    }
}
