<section id="header-top">
    <div class="wrapar">
      <div class="header">
        <div class="header-top" style="background-color: {{ $org->themecolor ?? '' }}">
          <div class="container">
            <div class="call pull-left">
              <p>Call us today : <span>{{ $org->contactno1 ?? ''}}</span> or <span>{{ $org->contactno2 ?? ''}}</span>
              </p>
            </div>
            @isset($org->canreg)
            <div class="user-info pull-right">
            <div class="user" >
            <ul>
            @guest
            <li><a href="{{ route('login') }}" >Login</a></li>
            @if($org->mustapprove == 0)
              <li><a href="{{ route('register') }}" >Register</a></li>
            @else
              <li><a href="{{ route('reqreg') }}" >Register</a></li>
            @endif
            @else
            <li><a href="{{ route('account') }}" >Welcome, {{ auth()->user()->fname }}
            <li><a href="{{ route('logout') }}" >Logout</a></li>
            @endauth
            </ul>
            </div>
            </div>
            @endisset
          </div>
        </div>
      </div>
    </div>
  </section>