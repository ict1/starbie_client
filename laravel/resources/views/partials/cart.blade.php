<div class="col-md-3 header-right">
          <div class="cart">
            
            <a aria-expanded="false" aria-haspopup="true" role="button" data-toggle="dropdown" class="dropdown-toggle"
              href=""><span><div class="cart-icon dropdown" ></div> </span> My Shopping Bag </a>
            <ul class="dropdown-menu pull-right cart-dropdown-menu">
              <li>
                <table class="table table-striped">
                  <tbody>
                    <tr>
                      <td class="text-center"><a href="product.html"><img class="img-thumbnail"
                            src="images/product/car3-70x92.jpg" alt="img"></a></td>
                      <td class="text-left"><a href="#">Black African Print Pencil Skirt</a></td>
                      <td class="text-right quality">X1</td>
                      <td class="text-right price-new">$254.00</td>
                      <td class="text-center"><button type="button" title="Remove" class="btn btn-xs remove"><i
                            class="fa fa-times"></i></button></td>
                    </tr>
                    <tr>
                      <td class="text-center"><a href="product.html"><img class="img-thumbnail"
                            src="images/product/car3-70x92.jpg" alt="img"></a></td>
                      <td class="text-left"><a href="#">Black African Print Pencil Skirt</a></td>
                      <td class="text-right quality">X1</td>
                      <td class="text-right price-new">$254.00</td>
                      <td class="text-center"><button type="button" title="Remove" class="btn btn-xs remove"><i
                            class="fa fa-times"></i></button></td>
                    </tr>
                  </tbody>
                </table>
              </li>
              <li>
                <div class="minitotal">
                  <table class="table pricetotal">
                    <tbody>
                      <tr>
                        <td class="text-right"><strong>Sub-Total</strong></td>
                        <td class="text-right price-new">$210.00</td>
                      </tr>
                      <tr>
                        <td class="text-right"><strong>Eco Tax (-2.00)</strong></td>
                        <td class="text-right price-new">$2.00</td>
                      </tr>
                      <tr>
                        <td class="text-right"><strong>VAT (20%)</strong></td>
                        <td class="text-right price-new">$42.00</td>
                      </tr>
                      <tr>
                        <td class="text-right"><strong>Total</strong></td>
                        <td class="text-right price-new">$254.00</td>
                      </tr>
                    </tbody>
                  </table>
                  <div class="controls"> <a class="btn btn-primary pull-left" href="cart.html" id="view-cart"><i
                        class="fa fa-shopping-cart"></i> View Cart </a> <a class="btn btn-primary pull-right"
                      href="checkout.html" id="checkout"><i class="fa fa-share"></i> Checkout</a> </div>
                </div>
              </li>
            </ul>
          </div>
        </div>