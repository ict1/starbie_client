<footer id="footer">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="newslatter">

                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3">
                <div class="new-store">
                    <h4>Get to Know Us</h4>
                    <ul class="toggle-footer">
                        <li><a href="{{ route('about') }}">About Us</a></li>

                    </ul>
                </div>
            </div>
            <div class="col-md-3">
                <div class="new-store">
                    <h4>My Profile</h4>
                    <ul class="toggle-footer">
                        @auth
                        <li><a href="#">My Wishlist</a></li>
                        <li><a href="#">My Orders</a></li>
                        <li><a href="#">My Cart</a></li>
                        @endauth
                        <li><a href="#">My Account</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-md-3">
                <div class="Customer service">
                    <h4>Customer Service</h4>
                    <ul class="toggle-footer">
                        <li><a href="{{ route('contact') }}">Contact Us</a></li>
                        <li><a href="{{ route('oreviews') }}">Reviews</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-md-3">
                <div class="Customer service">
                    <h4>Policies</h4>
                    <ul class="toggle-footer">
                        <li><a href="{{ route('TNC')}}">Terms & Conditions</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="footer-bottom">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="social-link">
                        <ul>
                            <li><a href="http://www.facebook.com"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="http://www.twitter.com"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="http://www.googleplus.com"><i class="fa fa-google-plus"></i></a></li>
                            <li><a href="http://www.linkedin.com"><i class="fa fa-linkedin"></i></a></li>
                            <li><a href="http://www.pinterest.com"><i class="fa fa-pinterest-p"></i></a></li>
                            <li><a href="http://www.instagram.com"><i class="fa fa-instagram"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="copy-right">
                        <p> All Rights Reserved. Copyright 2019. Powered by Starbie.</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer-offer">
            <h2 style="background-color:{{ $org->themecolor }}">Deal of the Day: Free Delivery over $50! Use Code FREE</h2>
        </div>
    </div>
</footer>

<script src="js/jQuery.js"></script>
  <script src="Bootstrap/js/bootstrap.js"></script>
  <script src="js/owl.carousel.min.js"></script>
  <script src="js/globle.js"></script>