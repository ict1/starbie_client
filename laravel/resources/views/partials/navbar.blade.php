<section>
    <div id="main-menu">
      <div class="container">
        <nav class="navbar navbar-default">
          <div class="navbar-header">
            <button aria-controls="navbar" data-target="#navbar" data-toggle="collapse" class="navbar-toggle collapsed"
              type="button"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span
                class="icon-bar"></span> <span class="icon-bar"></span> </button>
            <a href="#" class="navbar-brand">menu</a> </div>
          <div class="navbar-collapse collapse" id="navbar">
            <ul class="nav navbar-nav">
              <li><a href="{{ route('index')}}">HOME</a></li>
              <li><a href="{{ route('about') }}">ABOUT US</a></li>
              <li><a href="{{ route('contact') }}">CONTACT US</a></li>
              @auth
              <li><a href="#">MY ACCOUNT</a></li>
              <li><a href="#">MY ORDERS</a></li>
              <li><a href="#">WISHLIST</a></li>
              @endauth
              <li class="dropdown"> <a aria-expanded="false" aria-haspopup="true" role="button" data-toggle="dropdown"
                  class="dropdown-toggle" href="#"> CATEGORIES<span class="caret"></span></a>
                  <ul class="dropdown-menu">
                  @foreach($categories as $category)
                  <li><a href="{{ route('search') }}">{{ $category->name }}</a></li>
                  @endforeach
                </ul>
            </ul>
          </div>
        </nav>
      </div>
    </div>
  </section>