<head>
  <!-- Google Tag Manager -->
<script>
    (function(w, d, s, l, i) {
      w[l] = w[l] || [];
      w[l].push({
        'gtm.start': new Date().getTime(),
        event: 'gtm.js'
      });
      var f = d.getElementsByTagName(s)[0],
        j = d.createElement(s),
        dl = l != 'dataLayer' ? '&l=' + l : '';
      j.async = true;
      j.src =
        'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
      f.parentNode.insertBefore(j, f);
    })(window, document, 'script', 'dataLayer', 'GTM-T6D4V7J');
  </script>
  <!-- End Google Tag Manager -->
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>{{ ucfirst($org->name) }}</title>
  <meta content="Starbie" name="description">
  <meta content="Team 13" name="author">
  <link rel="apple-touch-icon" sizes="180x180" href="/icons/{{ $org->name }}/apple-touch-icon.png">
  <link rel="icon" type="image/png" sizes="32x32" href="/icons/{{ $org->name }}/favicon-32x32.png">
  <link rel="icon" type="image/png" sizes="16x16" href="/icons/{{ $org->name }}/favicon-16x16.png">
  <link rel="manifest" href="/icons/{{ $org->name }}/site.webmanifest">
  <link rel="shortcut icon" href="/icons/{{ $org->name }}/favicon.ico">
  <meta name="msapplication-TileColor" content="#da532c">
  <meta name="msapplication-config" content="/icons/{{ $org->name }}/browserconfig.xml">
  <meta name="theme-color" content="#ffffff">
  <link href="Bootstrap/css/bootstrap.css" rel="stylesheet">
  <link href="css/style.css" rel="stylesheet" type="text/css">
  <link href="css/font-awesome.css" rel="stylesheet" type="text/css">
  <link href='https://fonts.googleapis.com/css?family=Poppins:300,500,600,700' rel='stylesheet' type='text/css'>
  <link href="css/owl.carousel.css" rel="stylesheet" type="text/css">
  @yield('css')
  <meta property="og:url" content="{{ request()->url() }}" />
  @yield('og')
</head>
