<!-- SEARCH BAR FORM START -->
<div class="col-md-6 search_block">
    <div class="search">
        <form method="POST" action="{{ route('search') }}">
            @csrf
            <div class="search_cat">
                <select class="search-category" name="search-category"
                    style="border: 1px solid {{ $org->themecolor ?? ''}}"
                    name="category_id" id="category_id" onchange='if(this.value != 0) { this.form.submit(); }'>
                    <option selected>All Categories</option>
                    
                    @foreach($categories as $cat)
                    <option value="{{$cat->catid }}">{{ $cat->name }}</option>
                    @endforeach
                </select>
                <span class="fa fa-angle-down" style="pointer-events:none;"></span>
            </div>
            <input type="text" placeholder="Search..." style="border: 1px solid {{ $org->themecolor ?? ''}}">
            <button type="submit" class="btn submit"
                style="background: {{ $org->themecolor ?? ''}}; border: 1px solid {{ $org->themecolor ?? ''}}">
                <span class="fa fa-search"></span></button>
        </form>
    </div>
</div>
<!-- SEARCH BAR FORM END -->