<div class="user-info pull-right">
    <div class="user">
        @if (session('confirmation-success'))
        @component('front.components.alert')
        @slot('type')
        success
        @endslot
        {!! session('confirmation-success') !!}
        @endcomponent
        @endif
        @if (session('confirmation-danger'))
        @component('front.components.alert')
        @slot('type')
        error
        @endslot
        {!! session('confirmation-danger') !!}
        @endcomponent
        @endif
        <ul>
            <li><a href="#" data-toggle="modal" data-target="#login">Login</a>
                <!-- Modal -->
                <div class="modal fade" id="login" role="dialog">
                    <div class="modal-dialog">
                        <!-- Modal content-->
                        <div class="modal-content">
                            <div class="modal-header">
                                <div class="panel-heading">
                                    <div class="panel-title pull-left">Login</div>
                                    <div class="pull-right"><a href="#">Forgot password?</a>
                                        <button aria-hidden="true" data-dismiss="modal" class="close btn btn-xs "
                                            type="button">
                                            <i class="fa fa-times"></i> </button>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-body">
                                <form id="loginform" method="POST" class="form-horizontal"
                                    action="{{ route('login') }}">
                                    {{ csrf_field() }}
                                    @if ($errors->has('log'))
                                    @component('front.components.error')
                                    {{ $errors->first('log') }}
                                    @endcomponent
                                    @endif
                                    <div class="input-group"> <span class="input-group-addon"><i
                                                class="glyphicon glyphicon-user"></i></span>
                                        <input id="login-username" type="text" class="form-control" name="username"
                                            value="" placeholder="email">
                                    </div>
                                    <div class="input-group"> <span class="input-group-addon"><i
                                                class="glyphicon glyphicon-lock"></i></span>
                                        <input id="login-password" type="password" class="form-control" name="password"
                                            placeholder="password">
                                    </div>
                                    <div class="input-group">
                                        <div class="checkbox">
                                            <label>
                                                <input id="login-remember" type="checkbox" name="remember" {{
                                                    old('remember') ? 'checked' : '' }}>
                                                Remember me</label>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <!-- Button -->
                                        <div class="col-sm-12 controls"> <a id="btn-login" href="#"
                                                class="btn btn-primary btn-success">Login</a> <a id="btn-fblogin"
                                                href="#" class="btn btn-primary facebook">Login with</a> </div>
                                    </div>
                                </form>
                            </div>
                            <div class="modal-footer">
                                <div class="form-group">
                                    <div class="col-md-12 control">
                                        <div>Don't have an account! <a href="{{ route('register') }}">Sign Up Here</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </li>
            <li><a href="#" data-toggle="modal" data-target="#register">Register</a>
                <div class="modal fade" id="register" role="dialog">
                    <div class="modal-dialog">
                        <!-- Modal content-->
                        <div class="modal-content">
                            <div class="modal-header">
                                <div class="panel-heading">
                                    <div class="panel-title pull-left">Register</div>
                                    <div class="pull-right">
                                        <button aria-hidden="true" data-dismiss="modal" class="close" type="button"><i
                                                class="fa fa-times"></i> </button>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-body">
                                <div class="control-group">
                                    <!-- Username -->
                                    <label class="control-label" for="username">Username</label>
                                    <div class="controls">
                                        <input type="text" id="username" name="username" placeholder=""
                                            class="input-xlarge">
                                        <p class="help-block">Username can contain any letters or numbers, without
                                            spaces</p>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <!-- E-mail -->
                                    <label class="control-label" for="email">E-mail</label>
                                    <div class="controls">
                                        <input type="text" id="email" name="email" placeholder="" class="input-xlarge">
                                        <p class="help-block">Please provide your E-mail</p>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <!-- Password-->
                                    <label class="control-label" for="password">Password</label>
                                    <div class="controls">
                                        <input type="password" id="password" name="password" placeholder=""
                                            class="input-xlarge">
                                        <p class="help-block">Password should be at least 4 characters</p>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <!-- Password -->
                                    <label class="control-label" for="password_confirm">Password (Confirm)</label>
                                    <div class="controls">
                                        <input type="password" id="password_confirm" name="password_confirm"
                                            placeholder="" class="input-xlarge">
                                        <p class="help-block">Please confirm password</p>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <!-- Button -->
                                    <div class="controls">
                                        <button class="btn btn-success">Register</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </li>
        </ul>
    </div>
</div>