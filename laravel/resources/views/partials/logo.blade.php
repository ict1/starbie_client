<div class="logo">
    <a href="{{ route('index') }}"><img src="{{ $org->logo ?? ''}}" alt="{{ $org->name ?? ''}}"></a>
</div>