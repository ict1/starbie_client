@extends('front.layout')
@section('content')
<div id="offer">
  <div class="container">
    <div class="offer" style="background-color: {{ $org->themecolor }} ">
      <marquee width="100%" direction="left" height="17px" style="color: white">
        "SALE is ON"&nbsp;&nbsp;&nbsp;&nbsp;HURRY!! Grab your offers now!
        @isset($offers)
        @foreach($offers as $offer)
        &nbsp;&nbsp;&nbsp;&nbsp;{{ $offer->offerdetails }}
        &nbsp;&nbsp;&nbsp;&nbsp;{{ $offer->offerposter }}
        @endforeach
        @endif
      </marquee>
    </div>
  </div>
</div>

<div id="bread-crumb">
  <div class="container">
    <div class="row">
      <div class="col-md-3 col-sm-3 col-xs-3">
        <div class="page-title">
          <h4></h4>
        </div>
      </div>
      <div class="col-md-9 col-sm-9 col-xs-9">
        <div class="bread-crumb">
          <ul>
            <li><a href="home">Home</a></li>
            <li>|</li>
            <li>Home</a></li>
          </ul>
        </div>
      </div>
    </div>
  </div>
</div>


<div id="product-category">
  <div class="container">
    <div class="row">
      <div class="col-md-3 col-sm-4">

        <div id="left">
          <div class="sidebar-block">
            <div class="sidebar-widget Category-block">
              <div class="sidebar-title">
                <h4> Categories</h4>
              </div>
              <ul class="title-toggle">
                <li><a href="clothesproducts.html">Clothes</a></li>
                <li><a href="shoesproducts.html">Shoes</a></li>
                <li><a href="gamingproducts.html">Gaming </a></li>
                <li><a href="accessoriesproducts.html">Accessories</a></li>
                <li><a href="electronicproducts.html">Electronics</a></li>
                <li><a href="furnitureproducts.html">Furniture </a></li>
                <li><a href="ornamentproducts.html">Ornaments</a></li>
                <li><a href="sweetproducts.html">Sweets</a></li>
              </ul>
            </div>
            <div class="sidebar-widget Price-range">
              <div class="sidebar-title">
                <label for="amount">Price range:</label>
                <input type="text" id="amount" readonly style="border:0; color:#4d90fe; font-weight:bold;">
                <div id="slider-range"></div>
              </div>
            </div>

            <div class="sidebar-widget banner-block">
              <div class="left-banner"> <a href="#"><img src="images/sale.png" alt="#"></a> </div>
            </div>

          </div>
        </div>

      </div>
      <div class="col-md-9 col-sm-8">

        <div id="right">
          <div class="category-banner"> <a href="#"><img src="images/clothes.jpg" alt="#"></a> </div>
          <div class="row">
            <div class="col-md-6">
              <div class="view">

              </div>
            </div>
            <div class="col-md-6">
              <div class="shoring pull-right">
                <div class="short-by">
                  <p>Sort By</p>
                  <div class="select-item">
                    <select id="sort">
                      <option value="1" selected="selected">Name (A to Z)</option>
                      <option value="2">Name(Z - A)</option>
                      <option value="3">price(low&gt;high)</option>
                      <option value="4">price(high &gt; low)</option>

                    </select>
                    <span class="fa fa-angle-down"></span> </div>
                </div>
                <div class="show-item">
                  <p>Show</p>
                  <div class="select-item">
                    <select>
                      <option value="" selected="selected">24</option>
                      <option value="">12</option>
                      <option value="">6</option>
                    </select>
                    <span class="fa fa-angle-down"></span> </div>
                </div>
              </div>
            </div>
          </div>
          @if ($products != null)
          <div class="product-grid-view">
            <ul id="product-list">
              @foreach ($products as $product)
              <li>
                <div class="item col-md-4 col-sm-6 col-xs-6">
                  <div class="product-block">
                    @if (file_exists('images/products/'.$product->name.'.png'))
                    <div class="image"> <a href="{{ route('product.show', $product->itemid) }}"><img
                          class="img-responsive" title="{{ $product->name }}" alt="{{ $product->name }}"
                          src="images/products/{{ $product->name }}.png"></a> </div>
                      @else
                      <div class="image"> <a href="{{ route('product.show', $product->itemid) }}"><img
                            class="img-responsive" title="{{ $product->name }}" alt="{{ $product->name }}"
                            src="images/products/{{ $product->name }}.jpg"></a>
                      </div>
                      @endif
                      <div class="product-details">
                        <div class="product-name">
                          <h4><a href="{{ route('product.show', $product->itemid) }}">{{ $product->name }}</a></h4>
                        </div>
                        <div class="price"> <span class="price-old">{{ $product->priceselling + 50 }}</span> <span
                            class="price-new">{{ $product->priceselling }}</span> </div>
                        <div class="product-hov">
                          <ul>
                            <li class="wish"><a href="#"></a></li>
                            <li class="addtocart"><a href="#">Add to Cart</a> </li>
                            <li class="compare"><a href="#"></a></li>
                          </ul>
                          <div class="review"> <span class="rate"> <i class="fa fa-star rated"></i> <i
                                class="fa fa-star rated"></i>
                              <i class="fa fa-star rated"></i> <i class="fa fa-star rated"></i> <i
                                class="fa fa-star"></i> </span>
                          </div>
                        </div>
              </li>
              @endforeach
            </ul>
          </div>
          @endif
        </div>
        <div class="row">
          <div class="pagination-bar">
            <ul>
              <li><a href="#"><i class="fa fa-angle-left"></i></a></li>
              <li class="active"><a href="#">1</a></li>
              <li><a href="#">2</a></li>
              <li><a href="#">3</a></li>
              <li><a href="#"><i class="fa fa-angle-right"></i></a></li>
            </ul>
          </div>
        </div>

      </div>
    </div>
  </div>
</div>
@stop