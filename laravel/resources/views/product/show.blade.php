@extends('front.layout')
@section('content')
<div id="offer">
  <div class="container">
    <div class="offer" style="background-color: {{ $org->themecolor }} ">
      <marquee width="100%" direction="left" height="17px" style="color: white">
      "SALE is ON"&nbsp;&nbsp;&nbsp;&nbsp;HURRY!! Grab your offers now!
        @isset($offers)
        @foreach($offers as $offer)
        &nbsp;&nbsp;&nbsp;&nbsp;{{ $offer->offerdetails }}
        &nbsp;&nbsp;&nbsp;&nbsp;{{ $offer->offerposter }}
        @endforeach
        @endif
      </marquee>
    </div>
  </div>
</div>

<div id="bread-crumb">
  <div class="container">
    <div class="row">
      <div class="col-md-3">
        <div class="page-title">
          <h4>{{ ucfirst($product->category()->first()->name) }}</h4>
        </div>
      </div>
      <div class="col-md-9">
        <div class="bread-crumb">
          <ul>
            <li><a href="{{ route('index') }}">Home</a></li>
            <li>|</li>
            <li>{{ ucfirst($product->category()->first()->name) }}</a></li>
          </ul>
        </div>
      </div>
    </div>
  </div>
</div>


<div id="product-category">
  <div class="container">
    <div class="row">
      <div class="col-md-3 col-sm-4">

        <div id="left">
          <div class="sidebar-block">
            <div class="sidebar-widget Category-block">
              <div class="sidebar-title">
                <h4> Categories</h4>
              </div>
              @isset($categories)
              <ul class="title-toggle">
              @foreach($categories as $cat)
                <li name="category_id" value="{{ $cat->catid }}"><a href=" route('search') ">{{ ucfirst($cat->name) }}</a></li>
              @endforeach
              </ul>
              @endif
            </div>

            <div class="sidebar-widget banner-block">
              <div class="left-banner"> <a href="#"><img src="images/sale.png" alt="#"></a> </div>
            </div>

          </div>
        </div>

      </div>
      <div class="col-md-9 col-sm-8">

        <div id="right">
          <div class="product-detail-view">
            <div class="row">
              <div class="col-md-6">
              @if (file_exists('images/products/'.$product->name.'.png'))
                <div class="sp-loading"><img src="images/products/{{ $product->name }}.png" alt=""><br>
                </div>
                @else
                <div class="sp-loading"><img src="images/products/{{ $product->name }}.jpg" alt=""><br>
                </div>
                @endif

              </div>
              <div class="col-md-6">
                <div class="product-detail-content">
                  <div class="product-name">
                    <h4 id="product-name"><a href="product-detail-view.html">{{ $product->name }}</a></h4>
                  </div>
                  <div class="review"> <span class="rate"> <i class="fa fa-star rated"></i> <i
                        class="fa fa-star rated"></i> <i class="fa fa-star rated"></i> <i class="fa fa-star rated"></i>
                      <i class="fa fa-star"></i> </span> {{ count($product->reviews()->get()) }} Review(s) | <a href="#">Add Your Review </a> </div>
                  <div class="price"> <span class="price-old"> $ {{ $product->priceselling * 2 }}</span> </br>
                  @if($product->offers()->where('bid', $branch->bid)->first() != NULL)
                  <span class="price-new">$ {{ $product->priceselling * (1 - $product->offers()->where('bid', $branch->bid)->first()->__get('discount percentage') / 100) }}</span> </div>
                  @else
                  <span class="price-new">$ {{ $product->priceselling }}</span> </div>
                  @endif
                  <div class="stock"><span>In stock : </span>{{ $product->amountshown == 1 ? $product->amountexisting :
                    $product->amountshown }} </div>
                  <div class="products-code"> <span>Product Code : </span>{{ $product->name }}</div>
                  <div class="product-discription"><span>Description</span>
                    <p>{{ $product->description }}</p>
                  </div>
                  <div class="Sort-by">
                    <label>Sort by</label>
                    <select class="selectpicker form-control" id="select-by-size">

                      <option selected="selected" value="#">37</option>
                      <option value="#">38</option>
                      <option value="#">39</option>
                      <option value="#">40</option>
                      <option value="#">41</option>
                      <option value="#">42</option>
                      <option value="#">43</option>
                    </select>
                  </div>
                  <div class="Color">
                    <label>Color</label>
                    <select class="selectpicker form-control" id="select-by-color">
                      <option selected="selected" value="#">Black</option>
                      <option value="#">Blue</option>
                      <option value="#">Green</option>
                      <option value="#">Orange</option>
                      <option value="#">White</option>
                    </select>
                  </div>
                  <div class="product-qty">
                    <label for="qty">Qty:</label>
                    <div class="custom-qty">
                      <button
                        onclick="var result = document.getElementById('qty'); var qty = result.value; if( !isNaN( qty ) &amp;&amp; qty &gt; 1 ) result.value--;return false;"
                        class="reduced items" type="button"> <i class="fa fa-minus"></i> </button>
                      <input type="text" class="input-text qty" title="Qty" value="1" maxlength="8" id="qty" name="qty">
                      <button
                        onclick="var result = document.getElementById('qty'); var qty = result.value; if( !isNaN( qty )) result.value++;return false;"
                        class="increase items" type="button"> <i class="fa fa-plus"></i> </button>
                    </div>
                  </div>
                  <div class="add-to-cart">
                    <button type="submit" class="btn btn-default">Add to Cart</button>
                  </div>
                  <ul class="add-links">
                    <li> <a class="add-to-wishlist" href="#"> <i class="fa fa-heart-o"></i> Add to Wishlist </a></li>
                    <li> <a class="link-compare" href="#"> <i class="fa fa-bar-chart"></i> Add to Compare </a></li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
          <div class="product-detail-tab">
            <div class="row">
              <div class="col-md-12">
                <div id="tabs">
                  <ul class="nav nav-tabs">
                    <li><a data-toggle="tab" class="tab-Description selected" title="Description"
                        href="#descriptiontab">Description</a></li>
                    <li><a data-toggle="tab" class="tab-Product-Tags" title="Product-Tags"
                        href="#producttab">Product-Tags</a></li>
                    <li><a data-toggle="tab" class="tab-Reviews" title="Reviews" href="#reviewtab">Reviews</a></li>
                  </ul>
                </div>
                @include('layouts.errors-and-messages')
                <div id="items">
                  <div class="tab-content">
                    <div id="descriptiontab" class="tab-pane fade in active">
                      <br>
                      <h3><strong>Description</strong></h3><br>
                      <br>
                      <p>{{ $product->description }}.</p>
                    </div>
                    <div id="producttab" class="tab-pane fade">
                      <br>
                      <h3><strong>Product-Tags</strong></h3><br>
                      <br>
                      <p>Most of its text is made up from sections 1.10.32–3 of Cicero's De finibus bonorum et malorum
                        (On the Boundaries of Goods and Evils; finibus may also be translated as purposes). Neque porro
                        quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit is the first
                        known version ("Neither is there anyone who loves grief itself since it is grief and thus wants
                        to obtain it"). It was found by Richard McClintock, a philologist, director of publications at
                        Hampden-Sydney College in Virginia; he searched for citings of consectetur in classical Latin
                        literature, a term of remarkably low frequency in that literary corpus.</p>
                    </div>
                    <div id="reviewtab" class="tab-pane fade">
                      <br>
                      <h3><strong>Reviews</strong></h3><br>
                      <br>
                      @foreach($product->reviews()->get() as $review)
                      <p>{{ $review->customer()->first()->fname}} says {{ $review->comment}}</p>
                      <hr />
                      <title>Fixed Top Navbar Example for Bootstrap</title>


                      @endforeach
                      <form method="POST" action="{{ route('product.review') }}">
                        {{ csrf_field() }}
                        <div class="form-group">
                          <label for="CommentDes">Comment</label>
                          <input type="text" class="form-control" id="CommentDes" name="comment"
                            placeholder="Enter Your Comment" required>
                        </div>
                        <input type="hidden" name="itemid" value="{{$product->itemid}}">
                        <button type="submit" class="btn btn-primary">Submit</button>
                      </form>
                    </div>

                  </div>
                </div>
              </div>
            </div>
          </div>

        </div>

      </div>
    </div>
  </div>
</div>
@stop