<!DOCTYPE html>
<html lang="en">

<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <title>Company</title>
  <meta content="" name="description">
  <meta content="" name="author">
  <link rel="shortcut icon" type="image/x-icon" href="images/favicon.ico">
  <link rel="icon" type="image/png" href="images/favicon.png">
  <link rel="apple-touch-icon" href="images/favicon.png">
  <link href="Bootstrap/css/bootstrap.css" rel="stylesheet">
  <link href="css/style.css" rel="stylesheet" type="text/css">
  <link href="css/font-awesome.css" rel="stylesheet" type="text/css">
  <link href='https://fonts.googleapis.com/css?family=Poppins:300,500,600,700' rel='stylesheet' type='text/css'>
  <link href="css/owl.carousel.css" rel="stylesheet" type="text/css">
</head>

<body id="index">
  <div class="container">
    <div class="row">
      @foreach($organizations as $org)
      <div class="col-xs-3 col-sm-4 col-md-4">
          <a href="{{ route('show', $org->oid) }}" >
            <img class="img-fluid" src="{{ $org->logo }}" alt="{{ $org->name }}">
          </a>
      </div>
      @endforeach
    </div>
  </div>
</body>