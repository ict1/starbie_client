@extends('front.layout')
@section('content')
<section id="oreviews">
    <div id="bread-crumb">
        <div class="container">
            <div class="row">
                <div class="col-md-3">
                    <div class="page-title">
                        <h4>Reviews</h4>
                    </div>
                </div>
                <div class="col-md-9">
                    <div class="bread-crumb">
                        <ul>
                            <li><a href="{{ route('index') }}">Home</a></li>
                            <li>|</li>
                            <li><a>Reviews</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="about-page-contain">
        <div id="about-page-contain">
            <div class="container">
                <div class="review"> <span class="rate"> <i class="fa fa-star rated"></i> <i
                            class="fa fa-star rated"></i> <i class="fa fa-star rated"></i> <i
                            class="fa fa-star rated"></i> <i class="fa fa-star"></i> </span>
                    {{ count($reviews) }} Review(s) | <a href="#">Add Your Review </a> </div>
                <div id="reviews">
                    <div class="col-md-12">@include('layouts.errors-and-messages')</div>
                    <div class="col-md-4">
                        <ul>
                            @foreach($reviews as $r)
                            <li>{{ $r->comment }}</li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection