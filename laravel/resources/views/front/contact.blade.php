@extends('front.layout')

@section('content')

  <div id="bread-crumb">
    <div class="container">
      <div class="row">
        <div class="col-md-3 col-sm-3 col-xs-3">
          <div class="page-title">
            <h4>Contact Us</h4>
          </div>
        </div>
        <div class="col-md-9 col-sm-9 col-xs-9">
          <div class="bread-crumb">
            <ul>
              <li><a href="{{route('index')}}">Home</a></li>
              <li>|</li>
              <li><a href="{{route('contact')}}">Contact Us</a></li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div>

  
  <div id="contact-page-contain">
    <div class="container">
      <div class="row">
        
          <p class="text-center" style="font-size: 20px ;"  > We welcome you to contact us using the below details. </p>
        <br>
          <div class="col-lg-12 col-sm-12">
          <div class="map">
          <iframe
  width="100%"
  height="450"
  frameborder="0" style="border:0"
  src="https://www.google.com/maps/embed/v1/place?key={{ config('globalsettings.API_KEY') }}
    &q={{ $branch->address()->first()->country}}, {{ $branch->address()->first()->city}}" allowfullscreen>
</iframe>
            </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-offset-2 col-md-8">
          <div class="contact-title">
            <h2 class="tf">Contact</h2>
            <p class="text-center" style="font-size: 20px ; font-family: poppins" > We would love to hear from you! </p>
          </div>
          @include('layouts.errors-and-messages')
        </div>
      </div>
      <div class="contact-submit">
        <form method="POST" action="{{route('contact') }}">
          @CSRF
          <div class="row">
            <div class="col-md-6 col-sm-12">
              <div class="input-group">
                <input type="text" class="form-control" placeholder="First Name *" required>
              </div>
              <!-- /input-group -->
              <div class="input-group">
                <input type="email" class="form-control" placeholder="E-mail *" required>
              </div>
              <!-- /input-group --> 
            </div>
            <div class="col-md-6 col-sm-12">
              <div class="input-group">
                <input type="text" class="form-control" placeholder="Last Name *" required >
              </div>
              <!-- /input-group -->
              <div class="input-group">
                <input type="text" class="form-control" placeholder="Subject *" required>
              </div>
              <!-- /input-group --> 
            </div>
            <div class="col-md-12">
              <div class="input-group">
                <textarea class="form-control" name="contact-message" id="textarea_message" placeholder="Message *"required></textarea>
              </div>
              <div class="col-md-12 text-center">
                  <button class="btn btn-primary" type="submit" ><i class="fa fa-envelope-o"></i> Send </button>
              </form>
              </div>
            </div>
          </div>
        </form>
      </div>
      <div class="row">
        <div class="col-md-4">
          <div class="address">
            <h2 class="tf"><i class="fa fa-map-marker"></i></h2>
            <div class="address-info">{{ $branch->address()->first()->country}}, {{ $branch->address()->first()->city}}</div>
          </div>
        </div>
        <div class="col-md-4">
          <div class="complaint">
            <h2 class="tf"><i class="fa fa-mobile"></i></h2>
            <div class="call-info">Phone: {{ $org->contactno1 }} or {{ $org->contactno1 }}<br>
              <span>Monday to Friday (9 am to 6 pm)</span> </div>
          </div>
        </div>
        <div class="col-md-4">
          <div class="feedback">
            <h2 class="tf"><i class="fa fa-envelope"></i></h2>
            <div class="email-info"> <a href="#">xyz@abc.com</a><br>
          </div>
        </div>
      </div>
    </div>
  </div>
  
@Stop