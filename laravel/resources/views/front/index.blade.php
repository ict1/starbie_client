@extends('front.layout')

@section('content')
    <div id="banner">
        <div class="container">
        <div class="row">
            <div class="col-md-12">
            <div id="main-slider" class="owl-carousel">
                @isset($offers)
                    <ul>
                    @foreach($offers as $offer)
                    @if(file_exists('images/products/'.$offer->inventoryitem()->first()->name.'jpg'))
                    <li>
                        <div class="item"><a href="{{ route('product.show', $offer->inventoryitem()->first()->itemid) }}">
                        <img src="images/products/{{ $offer->inventoryitem()->first()->name }}.jpg" alt="{{ $offer->offername }}"></a></div>
                    </li>
                    @else
                    <li>
                        <div class="item"><a href="{{ route('product.show', $offer->inventoryitem()->first()->itemid) }}">
                        <img src="images/products/{{ $offer->inventoryitem()->first()->name }}.png" alt="{{ $offer->offername }}"></a></div>
                    </li>
                    @endif
                    @endforeach
                    </ul>
                @else
                    <ul>
                        <li>
                            <div class="item"><img src="images/placeholder.png" alt="placeholder"></div>
                        </li>
                    </ul>
                @endisset
            </div>
            </div>
        </div>
        </div>
    </div>
    <br>
    <br> 
@endsection