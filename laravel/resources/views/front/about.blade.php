@extends('front.layout')

@section('content')

<div id="bread-crumb">
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <div class="page-title">
                    
                </div>
            </div>
            <div class="col-md-9">
                <div class="bread-crumb">
                    <ul>
                        <li><a href="{{ route('index') }}">Home</a></li>
                        <li>|</li>
                        <li><a>About Us</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="about-page-contain">
    <div id="about-page-contain">
        <div class="container">



            <div class="product-detail-tab">
                <div class="row">
                    <div class="col-md-12">
                        <div id="tabs">
                            <ul class="nav nav-tabs">
                                <li><a data-toggle="tab" class="tab-Description selected" title="Description"
                                        href="#historytab">About Us</a></li>

                            </ul>
                        </div>
                        <div id="items">
                            <div class="tab-content">
                                <div id="historytab" class="tab-pane fade in active">
                                    <br>
                                    <h3><strong>{{ $branch->about }}</strong></h3><br>
                                    <br>
                                    <p>{{ $org->about }}</p>
                                </div>
                                <br><br>
 
</div>
@endsection
<?php view()->share('branch', $branch); ?>