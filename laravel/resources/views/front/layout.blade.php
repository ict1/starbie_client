<!DOCTYPE html>
<html lang="en">

@include('partials.head')

<body id="index">
  <!-- Google Tag Manager (noscript) -->
  <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-T6D4V7J" height="0" width="0"
      style="display:none;visibility:hidden"></iframe></noscript>
  <!-- End Google Tag Manager (noscript) -->
  <noscript>
    <p class="alert alert-danger">
      You need to turn on your javascript. Some functionality will not work if this is disabled.
      <a href="https://www.enable-javascript.com/" target="_blank">Read more</a>
    </p>
  </noscript>
  <!-- Header Start-->
  @include('partials.header-top')
  <section id="header-mid">
    <div class="header-mid">
      <div class="container">
        <div class="row">
          <div class="col-md-3 header-left">
            @include('partials.logo')
            <form method="POST" action="{{ route('branch') }}">
              @csrf
              <select name="branch_id" id="branch_id" onchange='if(this.value != 0) { this.form.submit(); }'>
                @foreach($branches as $b)
                @if($b->bid == $branch->bid)
                <option value="{{$b->bid }}" selected="selected">{{ $b->about }}</option>
                @else
                <option value="{{$b->bid }}">{{ $b->about }}</option>
                @endif
                @endforeach
              </select>
            </form>
          </div>
          @include('partials.searchbar')
          <div class="pull-right"><a aria-expanded="false" aria-haspopup="true" role="button" data-toggle="dropdown" class="dropdown-toggle"
              href=""><span><div class="cart-icon" ></div> </span> My Shopping Bag </a></div>
        </div>
      </div>
    </div>
  </section>
  <!-- Navbar Start-->
  @include('partials.navbar')
  
  @yield('content')

  @include('partials.footer')

  @yield('extra-js')
</body>


</html>