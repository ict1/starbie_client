var products = [{
	name:"Milk Flavour Sweet",
	oldPrice: 5.00,
	newPrice: 4.00,
	image: "images/product/milk.jpg",
	link: "SweetsDetail.html"
 }, { 
	name:"Strawberry Flavour Sweet",
	oldPrice: 4.00,
	newPrice: 3.00,
	image: "images/product/strawberry.jpg",
	link: "SweetsDetail.html"
 }, {
	name:"Blueberry Flavour Sweet",
	oldPrice: 4.00,
	newPrice: 3.00,
	image: "images/product/blueberry.jpg",
	link: "SweetsDetail.html"
 }, {
	name:"Watermelon Flavour Sweet",
	oldPrice: 5.00,
	newPrice: 4.00,
	image: "images/product/watermelon.jpg",
	link: "SweetsDetail.html"
 }, {
	name:"Sweet Melon Flavour Sweet",
	oldPrice: 4.50,
	newPrice: 3.50,	
	image: "images/product/sweetmelon.jpg",
	link: "SweetsDetail.html"
 }, {
	name:"Orange Flavour Sweet",
	oldPrice: 4.00,
	newPrice: 3.50,
	image: "images/product/orange.jpg",
	link: "SweetsDetail.html"
 }, {
	name:"Apple Flavour Sweet",
	oldPrice: 4.00,
	newPrice: 3.50,
	image: "images/product/apple.jpg",
	link: "SweetsDetail.html"
 }, {
	name:"Bear Gummies",
	oldPrice: 6.00,
	newPrice: 5.50,
	image: "images/product/bear.jpg",
	link: "SweetsDetail.html"
 }, {
	name:"Snake Gummies",
	oldPrice: 5.00,
	newPrice: 4.50,
	image: "images/product/snake.jpg",
	link: "SweetsDetail.html"
 }];

$(function() {
	$("#sort").change(function() {
		if ($("#sort").val() == 1) {
			products.sort(function(a, b) {
              			return a.name.localeCompare(b.name);
          		});
		} else if ($("#sort").val() == 2) {
      			products.sort(function(a, b) {
         			return b.name.localeCompare(a.name);
     			});
   		} else if ($("#sort").val() == 3) {
      			products.sort(function(a, b) {
         			return a.newPrice - b.newPrice;
     			});     
   		} else if ($("#sort").val() == 4) {
      			products.sort(function(a, b) {
					 return b.newPrice - a.newPrice;					
				 });
				 
   		}

		console.log(products);

		var productsHtml = '';

		for (var product of products) {
			var productHtml = '<li>' +
				'<div class="item col-md-4 col-sm-6 col-xs-6">' +
				'<div class="product-block">' +
				'<div class="image"> <a href= " ' + product.link + ' "><img class="img-responsive" title="T-shirt" alt="T-shirt" src="' + product.image + '"></a> </div>' +
				'<div class="product-details">' +
				'<div class="product-name">' +
				'<h4><a href= " ' + product.link + ' ">' + product.name + '</a></h4>' +
				'</div>' +
				'<div class="price"> <span class="price-old">$' + product.oldPrice + '</span> <span class="price-new">' + product.newPrice + '</span> </div>' +
				'<div class="product-hov">' +
				'<ul>' +
				'<li class="wish"><a href="#"></a></li>' +
				'<li class="addtocart"><a href="#">Add to Cart</a> </li>' +
				'<li class="compare"><a href="#"></a></li>' +
				'</ul>' +
				'<div class="review"> <span class="rate"> <i class="fa fa-star rated"></i> <i class="fa fa-star rated"></i> <i class="fa fa-star rated"></i> <i class="fa fa-star rated"></i> <i class="fa fa-star"></i> </span> </div>' +
				'</div>' +
				'</div>' +
				'</div>' +
				'</div>' +
				'</li>';
			
			productsHtml += productHtml;
		}

		$('#product-list').html(productsHtml);
	});
	$('#sort').change();
});
