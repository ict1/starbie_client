var products = [{
	name:"Gaming keyboards",
	oldPrice: 123.20,
	newPrice: 100.00,
	image: "images/product/keyboard.jpg",
	link: "GamingDetail.html"
 }, { 
	name:"Gaming Mouse",
	oldPrice: 100,
	newPrice: 50.99,
	image: "images/product/mouse.jpg",
	link: "GamingDetail.html"
 }, {
	name:"Games",
	oldPrice: 123.20,
	newPrice: 40.99,
	image: "images/product/game.jpg",
	link: "GamingDetail.html"
 }, {
	name:"PS4",
	oldPrice: 500.00,
	newPrice: 300.00,
	image: "images/product/ps4.jpg",
	link: "GamingDetail.html"
 }, {
	name:"Xbox",
	oldPrice: 450.00,
	newPrice: 250.00,	
	image: "images/product/xbox.jpg",
	link: "GamingDetail.html"
 }, {
	name:"Gaming PC",
	oldPrice: 1550.00,
	newPrice: 1199.99,
	image: "images/product/pc.jpg",
	link: "GamingDetail.html"
 }, {
	name:"Gaming Laptop",
	oldPrice: 1600.00,
	newPrice: 1399.99,
	image: "images/product/laptop.jpg",
	link: "GamingDetail.html"
 }, {
	name:"Gaming Headset",
	oldPrice: 123.20,
	newPrice: 99.99,
	image: "images/product/headset.jpg",
	link: "GamingDetail.html"
 }, {
	name:"Gaming Mousepads",
	oldPrice: 50.00,
	newPrice: 29.99,
	image: "images/product/mousepad.jpg",
	link: "GamingDetail.html"
 }];

$(function() {
	$("#sort").change(function() {
		if ($("#sort").val() == 1) {
			products.sort(function(a, b) {
              			return a.name.localeCompare(b.name);
          		});
		} else if ($("#sort").val() == 2) {
      			products.sort(function(a, b) {
         			return b.name.localeCompare(a.name);
     			});
   		} else if ($("#sort").val() == 3) {
      			products.sort(function(a, b) {
         			return a.newPrice - b.newPrice;
     			});     
   		} else if ($("#sort").val() == 4) {
      			products.sort(function(a, b) {
					 return b.newPrice - a.newPrice;					
				 });
				 
   		}

		console.log(products);

		var productsHtml = '';

		for (var product of products) {
			var productHtml = '<li>' +
				'<div class="item col-md-4 col-sm-6 col-xs-6">' +
				'<div class="product-block">' +
				'<div class="image"> <a href= " ' + product.link + ' "><img class="img-responsive" title="T-shirt" alt="T-shirt" src="' + product.image + '"></a> </div>' +
				'<div class="product-details">' +
				'<div class="product-name">' +
				'<h4><a href= " ' + product.link + ' ">' + product.name + '</a></h4>' +
				'</div>' +
				'<div class="price"> <span class="price-old">$' + product.oldPrice + '</span> <span class="price-new">' + product.newPrice + '</span> </div>' +
				'<div class="product-hov">' +
				'<ul>' +
				'<li class="wish"><a href="#"></a></li>' +
				'<li class="addtocart"><a href="#">Add to Cart</a> </li>' +
				'<li class="compare"><a href="#"></a></li>' +
				'</ul>' +
				'<div class="review"> <span class="rate"> <i class="fa fa-star rated"></i> <i class="fa fa-star rated"></i> <i class="fa fa-star rated"></i> <i class="fa fa-star rated"></i> <i class="fa fa-star"></i> </span> </div>' +
				'</div>' +
				'</div>' +
				'</div>' +
				'</div>' +
				'</li>';
			
			productsHtml += productHtml;
		}

		$('#product-list').html(productsHtml);
	});
	$('#sort').change();
});
